import React from 'react';
import ReactDOM from 'react-dom';
import './sass/main.scss';
import {BrowserRouter} from 'react-router-dom'
import 'react-toastify/dist/ReactToastify.css';
import { toast } from 'react-toastify';

import App from './components/App';

toast.configure({
    autoClose: 4000,
    draggable: false,
    position: toast.POSITION.BOTTOM_RIGHT,
  });

ReactDOM.render(
<BrowserRouter>
<App />
</BrowserRouter>
    , document.getElementById('root'));
