export const successOptions = {
	className: 'toast--success',
	bodyClassName: 'toastBody--success',
};

export const errorOptions = {
	className: 'toast--error',
	bodyClassName: 'toastBody--error',
};
