const strings = {
    "api_url":'http://127.0.0.1:8000/',
        "data": [
        {
            "name": "Dmc Jupiter",
            "age": 2007,
            "deadweight": 24823,
            "open_port": "Vizag",
            "date": "2020-08-29",
            "eta_loadport": "2020-09-02 to\n2020-09-03",
            "cargo_intake": 24723.82,
            "fuel_cost": 138359.47,
            "voyage_duration": 22.22,
            "breakeven_hire_rate": 17298.98,
            "freight_rate": 25.92,
            "profit_loss": -60024.87
        },
        {
            "name": "Tan Binh 135",
            "age": 2001,
            "deadweight": 28492,
            "open_port": "Yangon",
            "date": "2020-08-30",
            "eta_loadport": "2020-09-02 to\n2020-09-02",
            "cargo_intake": 27684.76,
            "fuel_cost": 124847.23,
            "voyage_duration": 22.11,
            "breakeven_hire_rate": 21225.99,
            "freight_rate": 22.59,
            "profit_loss": 27111.95
        },
        {
            "name": "Rivertec",
            "age": 2010,
            "deadweight": 32922,
            "open_port": "Chittagong",
            "date": "2020-08-29",
            "eta_loadport": "2020-09-02 to\n2020-09-04",
            "cargo_intake": 30946.96,
            "fuel_cost": 159796.47,
            "voyage_duration": 23.81,
            "breakeven_hire_rate": 21535.58,
            "freight_rate": 22.4,
            "profit_loss": 36569.81
        },
        {
            "name": "Tan Binh 259",
            "age": 2005,
            "deadweight": 31901,
            "open_port": "Chittagong",
            "date": "2020-08-30",
            "eta_loadport": "2020-09-03 to\n2020-09-03",
            "cargo_intake": 31589.81,
            "fuel_cost": 136464.48,
            "voyage_duration": 23.35,
            "breakeven_hire_rate": 23628.12,
            "freight_rate": 20.93,
            "profit_loss": 84727.32
        }
    ],
    "success": true
}

export default strings