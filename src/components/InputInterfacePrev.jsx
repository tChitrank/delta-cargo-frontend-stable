import React, { useState, useEffect } from 'react';
import { Form, Field } from 'react-final-form';
import createDecorator from 'final-form-calculate'
import TextInput from './form-components/TextInput';
import TextInputSuggestion from './form-components/TextInputSuggestion';
import TextInput1D from './form-components/TextInput1D';
import TextInput2Drop from './form-components/TextInput2Drop';
import TextInput1Drop from './form-components/TextInput1Drop';
import SelectInput from './form-components/SelectInput';
import Separator from './form-components/Separator';
import ExternalModificationDetector from './form-components/ExternalModificationDetector'
import BooleanDecay from './form-components/BooleanDecay'
import { toast } from 'react-toastify';
import {errorOptions} from '../toastConfig'
import IdleModal from './modal/IdleModal'

//Dummy data import
import strings from '../assets/strings'
//import validateForm from './validateForm'

// Choices of input units
import {
  STOWAGE_FACTOR as sf,
  DRAFTS as drafts,
  DURATION_RATE_TYPES as types,
  CARGO as cargos,
  ZONE as zone,
  BBI_HANDY_INDEX as bbi_handy,
  BBI_PANAMAX_INDEX as bbi_panamax,
  BBI_SUPRA_INDEX as bbi_supra,
  BUNKER_PORTS as bunker_ports,
  PORT_TERMS as port_terms
} from '../InputUnitChoices';

const InputInterfacePrev = (props) => {
  // const [stowageFactor, setStowageFactor] =useState(undefined)

  // const [cargo,updateCargo] = useState(null)
  const [loading, setLoading] = useState(false);
  const [SFUnit, setSFUnit] = useState(sf[1]);
  const [loadType, setLoadType] = useState(types[0]);
  const [loadPortDraft, setLoadPortDraft] = useState(drafts[0].key);
  const [loadPortDraftSelection, setLoadPortDraftSelection] = useState(drafts[0].text);
  //const [loadTerm, setLoadTerm] = useState(terms[0]);
  const [loadTermDrop, setLoadTermDrop] = useState(port_terms[0].key);
  const [loadTermSelection, setLoadTermSelection] = useState(port_terms[0].text);

  const [dischargeType, setDischargeType] = useState(types[0]);
  const [dischargePortDraft, setDischargePortDraft] = useState(drafts[0].key);
  const [dischargePortDraftSelection, setDischargePortDraftSelection] = useState(drafts[0].text);
  //const [dischargeTerm, setDischargeTerm] = useState(terms[0]);
  const [dischargeTermDrop, setDischargeTermDrop] = useState(port_terms[0].key);
  const [dischargeTermSelection, setDischargeTermSelection] = useState(port_terms[0].text);

  //const [bunkerPortDraft, setBunkerPortDraft] = useState(drafts[0]);
  const userData = JSON.parse(localStorage.getItem('UserInfo'))

  let formValues = JSON.parse(localStorage.getItem('formData'))
  const [bbiType, setBbiType] = useState(bbi_handy[0].key);
  const [bbiTypeSelection, setBbiTypeSelection] = useState(formValues.baltic_type);
  const [bbiChoices, updateBbiChoices] = useState([]);

  useEffect(() => {
    if(formValues.vessel_segment==='Handy'){
      updateBbiChoices(bbi_handy)
    }  else if (formValues.vessel_segment === 'Supra') {
      updateBbiChoices(bbi_supra)
    }
    else if (formValues.vessel_segment === 'Panamax') {
      updateBbiChoices(bbi_panamax)
    }
  }, [])

  const onSubmit = (values, e) => {
    // console.log("values ", values)
    // e.preventDefault()
   setLoading(true)
   window.scrollTo(0,0)

  let cargoQuanityValue = ""
  let s = values.cargoQuantity.toString()
  if(s.charAt(s.length-1)==='k'){
    cargoQuanityValue = s.replace('k', '000')
  }
  else if(s.charAt(s.length-1)==='K'){
    cargoQuanityValue = s.replace('K', '000')
  }
  else{
    cargoQuanityValue = s
  }

   let loadPortDraftValue = ""
   let dischargePortDraftValue = ""

    if (loadPortDraftSelection === 'SW') {
      loadPortDraftValue = values.loadPortDraft
    } else if (loadPortDraftSelection === 'BW') {
      loadPortDraftValue = values.loadPortDraft * (1.015 / 1.025)
    } else if (loadPortDraftSelection === 'FW') {
      loadPortDraftValue = values.loadPortDraft * (1 / 1.025)
    }

    if (dischargePortDraftSelection === 'SW') {
      dischargePortDraftValue = values.dischargePortDraft
    } else if (dischargePortDraftSelection === 'BW') {
      dischargePortDraftValue = values.dischargePortDraft * (1.015 / 1.025)
    } else if (dischargePortDraftSelection === 'FW') {
      dischargePortDraftValue = values.dischargePortDraft * (1 / 1.025)
    }

    let updateShipZone = values.shipZone.map((obj) => obj.value)
    // console.log("shipZone",updateShipZone)
    const formData =
    {
      vessel_segment: values.shipSegment,
      vessel_loading_zone: updateShipZone,
      load_port: values.loadPort,
      discharge_port: values.dischargePort,
      bunker_port: values.bunkerPort.value,
      bunker_duration: parseFloat(values.bunkerDuration),
      layday_comm: values.layDate,
      lay_cancel: values.canDate,
      cargo_quantity: parseFloat(cargoQuanityValue),
      stowage_factor: parseFloat(values.stowageFactor),
      cargo_variation: parseFloat(values.cargoTolerance),
      commission: parseFloat(values.commission),
      loadport_draft: parseFloat(loadPortDraftValue),
      dischargeport_draft: parseFloat(dischargePortDraftValue),
      sea_margin: parseFloat(values.seaMargin),
      load_term: loadTermSelection,
      discharge_term: dischargeTermSelection,
      load_rate: parseFloat(values.loadType),
      discharge_rate: parseFloat(values.dischargeType),
      loadport_cost: parseFloat(values.loadPortCost),
      dischargeport_cost: parseFloat(values.dischargePortCost),
      bunkerport_cost: parseFloat(values.costBunkerPort),
      freight_rate: parseFloat(values.freightRate),
      fuel_rate: parseFloat(values.fuelRate),
      gas_rate: parseFloat(values.gasRate),
      benchmark_baltic_index: parseFloat(values.benchMarkBalticIndex),
      baltic_type: bbiTypeSelection,
      cargo_category:values.cargoCategory.value,
      ballast_port: values.ballastPort
    }
    //console.log('formdata', formData)
   localStorage.setItem('formData', JSON.stringify(formData))
    localStorage.setItem('requestData', JSON.stringify(formData))

    // localStorage.setItem('responseData',JSON.stringify(strings.data))
    // props.history.push('/Results')
    console.log("entering api", formData)
    fetch(strings.api_url+'api/vessel_search/', {
      method: 'POST',
      headers: {
        DELTAAUTH: userData.token,
        'Accept': 'application/json',
        'Content-type': 'application/json',
      },
      body: JSON.stringify({
        vessel_segment: values.shipSegment,
        vessel_loading_zone: updateShipZone,
        load_port: values.loadPort,
        discharge_port: values.dischargePort,
        bunker_port: values.bunkerPort.value,
        bunker_duration: parseFloat(values.bunkerDuration),
        layday_comm: values.layDate,
        lay_cancel: values.canDate,
        cargo_quantity: parseFloat(cargoQuanityValue),
        stowage_factor: parseFloat(values.stowageFactor),
        cargo_variation: parseFloat(values.cargoTolerance),
        commission: parseFloat(values.commission),
        loadport_draft: parseFloat(loadPortDraftValue),
        dischargeport_draft: parseFloat(dischargePortDraftValue),
        sea_margin: parseFloat(values.seaMargin),
        load_term: loadTermSelection,
        discharge_term: dischargeTermSelection,
        load_rate: parseFloat(values.loadType),
        discharge_rate: parseFloat(values.dischargeType),
        loadport_cost: parseFloat(values.loadPortCost),
        dischargeport_cost: parseFloat(values.dischargePortCost),
        bunkerport_cost: parseFloat(values.costBunkerPort),
        freight_rate: parseFloat(values.freightRate),
        fuel_rate: parseFloat(values.fuelRate),
        gas_rate: parseFloat(values.gasRate),
        benchmark_baltic_index: parseFloat(values.benchMarkBalticIndex),
        baltic_type: bbiTypeSelection,
        ballast_port: values.ballastPort
      })
    })
      .then(response => {
        return response.json();
      })
      .then(data => {
        console.log('dta', data.data);
        if(data.success===true){
          setLoading(false)
        localStorage.setItem('responseData', JSON.stringify(data.data))
        props.history.push('/results')
        }
        else{
          setLoading(false)
          toast.error(data.data,errorOptions)
          throw Error(data.data)
        }
      })
      .catch(err => console.log('error',err )); 

  };

  const cargoDecorator = createDecorator(
    {
      field: /cargoCategory/, // when a field matching this pattern changes...
      updates: {
        // ...update the total to the result of this function
        stowageFactor: (ignoredValue, allValues) => {
          return fetch(strings.api_url+`api/cargo_details/?cargo=${allValues.cargoCategory.value}`, {
            method: 'GET',
            headers: {
              DELTAAUTH: userData.token,
              'Accept': 'application/json',
              'Content-type': 'application/json',
            },
          })
            .then(response => {
              if (!response || !response.ok) throw alert("Chitrank");
              return response.json();
            })
            .then(data => {
              // console.log('dta',data.data[0].sf_ft);
              return data.data[0].sf_ft
            })
            .catch(err => console.log('Error Retreiving', err));
        }
      }
    },
    {
      field: /benchMarkBalticIndex/, // when a field matching this pattern changes...
      updates: {
        // ...update the total to the result of this function
        ballastPort: (ignoredValue, allValues) => {
          //console.log('ballastPort',bbiTypeSelection)
          if (bbiTypeSelection==='P1A_82' || bbiTypeSelection==='P2A_82') {
            return 'GIBRALTAR'
          }
          else if (bbiTypeSelection==='P3A_82' || bbiTypeSelection==='P4A_82') {
            return 'YOSU'
          }
          else if (bbiTypeSelection==='P5_82' || bbiTypeSelection==='S8_58' || bbiTypeSelection==='S10_58') {
            return 'HONG KONG'
          }
          else if (bbiTypeSelection==='P6_82' || bbiTypeSelection==='HS5_38') {
            return 'SINGAPORE'
          }
          else if (bbiTypeSelection==='HS1_38' || bbiTypeSelection==='HS2_38') {
            return 'SKAW'
          }
          else if (bbiTypeSelection==='HS3_38') {
            return 'RIO DE JANEIRO'
          }
          else if (bbiTypeSelection==='HS4_38' || bbiTypeSelection==='S4A-58' || bbiTypeSelection==='S1C_58') {
            return 'SW PASS'
          }
          else if (bbiTypeSelection==='HS6_38' || bbiTypeSelection==='HS7_38' || bbiTypeSelection==='S2_58'  || bbiTypeSelection==='S3_58') {
            return 'CJK'
          }
          else if (bbiTypeSelection==='S1B_58') {
            return 'CANAKKALE'
          }
          else if (bbiTypeSelection==='S4B-58') {
            return 'CAPE PASSERO'
          }
          else if (bbiTypeSelection==='S5_58' || bbiTypeSelection==='S9_58') {
            return 'ABIDJAN'
          }
          else if (bbiTypeSelection==='S11_58') {
            return 'SHANGHAI'
          }
        }
      }
    },
    {
      field: /bunkerPort/, // when a field matching this pattern changes...
      updates: {
        // ...update the total to the result of this function
        fuelRate: (ignoredValue, allValues) => {
          return fetch(strings.api_url+`api/fuel_gas_rate/`, {
            method: 'POST',
            headers: {
              DELTAAUTH: userData.token,
              'Accept': 'application/json',
              'Content-type': 'application/json',
            },
            body: JSON.stringify({
              "bunker_port": allValues.bunkerPort.value,
              "lay_cancel": document.getElementById('canDate').value
            })
          })
            .then(response => {
              if (!response || !response.ok) throw Error(response.status);
              return response.json();
            })
            .then(data => {
              // console.log('dta',data.data.fuel_rate);
              return data.data.fuel_rate
            })
            .catch(err => console.log('Error Retreiving', err));
        },
        gasRate: (ignoredValue, allValues) => {
          return fetch(strings.api_url+`api/fuel_gas_rate/`, {
            method: 'POST',
            headers: {
              DELTAAUTH: userData.token,
              'Accept': 'application/json',
              'Content-type': 'application/json',
            },
            body: JSON.stringify({
              "bunker_port": allValues.bunkerPort.value,
              "lay_cancel": document.getElementById('canDate').value
            })
          })
            .then(response => {
              if (!response || !response.ok) throw Error(response.status);
              return response.json();
            })
            .then(data => {
              // console.log('dta',data.data.fuel_rate);
              return data.data.gas_rate
            })
            .catch(err => console.log('Error Retreiving', err));
        },
      }
    }
  )
  
  return (
    <div>
      <Form onSubmit={onSubmit} decorators={[cargoDecorator]} //validate={validateForm}
      initialValues={{freightRate:parseFloat(formValues.freight_rate),
        costBunkerPort:formValues.bunkerport_cost,
        bunkerDuration:formValues.bunker_duration,
        shipSegment:formValues.vessel_segment,
        cargoCategory:{value:formValues.cargo_category,label:formValues.cargo_category},
        bunkerPort:{value:formValues.bunker_port,label:formValues.bunker_port},
        layDate:formValues.layday_comm,
        canDate:formValues.lay_cancel,
        cargoQuantity:formValues.cargo_quantity,
        stowageFactor:formValues.stowage_factor,
        cargoTolerance:formValues.cargo_variation,
        seaMargin:formValues.sea_margin,
        commission:formValues.commission,
        loadPort:formValues.load_port,
        dischargePort:formValues.discharge_port,
        loadPortDraft:formValues.loadport_draft,
        dischargePortDraft:formValues.dischargeport_draft,
        loadPortCost:formValues.loadport_cost,
        dischargePortCost:formValues.dischargeport_cost,
        loadType:formValues.load_rate,
        dischargeType:formValues.discharge_rate,
        gasRate:formValues.gas_rate,
        fuelRate:formValues.fuel_rate,
        benchMarkBalticIndex:formValues.benchmark_baltic_index,
        ballastPort:formValues.ballast_port,
        shipZone:formValues.vessel_loading_zone.map((zone) => ({ label: zone, value: zone }))
    }}
      >
        {({
          handleSubmit, form, submitting, pristine, values,
        }) => (
            <form onSubmit={handleSubmit} className="inputForm">
              <div className="inputForm__grid">
                <Separator title="Ship Details" />
                <TextInput
                  inputName="shipSegment"
                  inputId="shipSegment"
                  inputLabel="Ship Segment"
                  inputPlaceholder="Enter Ship Segment..."
                  isDisabled={true}
                />

                <SelectInput
                  inputName="shipZone"
                  inputId="shipZone"
                  inputLabel="Ship Zone"
                  inputPlaceholder="Enter Ship Zone..."
                  multipleSelection={true}
                  options={zone.map((zone) => ({ label: zone, value: zone }))}
                />

             {/*<TextInput1D
                  inputName="benchMarkBalticIndex"
                  inputId="benchMarkBalticIndex"
                  inputLabel="Benchmark Baltic Index"
                  inputPlaceholder="Enter Benchmark Baltic Index Value..."
                  unitChoices={bbiChoices}
                  selectedUnit={bbiType}
                  setSelectedUnit={setBbiType}
                /> */}    

                <Separator title="Cargo Details" />
                <TextInput
                  inputName="freightRate"
                  inputId="freightRate"
                  inputLabel="Freight Rate ($)"
                  inputPlaceholder="Enter Freight Rate..."
                />

                <div className="form-control">
                  {/* eslint-disable */}
                  <label htmlFor="layDate" className="inputLabel">Layday Comm</label>
                  <Field name="layDate">
                    {({ input, meta }) => (
                      <div style={{ width: '40rem', wordBreak: 'break-all' }}>
                        <input {...input} type="date" id="layDate" className="inputField" />
                        {meta.error && meta.touched && <p style={{ color: 'red' }}>{meta.error}</p>}
                      </div>
                    )}
                  </Field>
                  {/* eslint-enable */}
                </div>

                <div className="form-control">
                  {/* eslint-disable */}
                  <label htmlFor="canDate" className="inputLabel">Cancelling</label>
                  <Field name="canDate">
                    {({ input, meta }) => (
                      <div style={{ width: '40rem', wordBreak: 'break-all' }}>
                        <input {...input} type="date" id="canDate" className="inputField" />
                        {meta.error && meta.touched && <p style={{ color: 'red' }}>{meta.error}</p>}
                      </div>
                    )}
                  </Field>
                  {/* eslint-enable */}
                </div>

                <SelectInput
                  inputName="cargoCategory"
                  inputId="cargoCategory"
                  inputLabel="Cargo Category"
                  inputPlaceholder="Select Cargo Category..."
                  options={cargos.map((cargos) => ({ label: cargos, value: cargos }))}
                />

                <TextInput
                  inputName="cargoQuantity"
                  inputId="cargoQuantity"
                  inputLabel="Cargo Quantity (MT)"
                  inputPlaceholder="Enter cargo quantity..."
                />

                <ExternalModificationDetector name="stowageFactor">
                  {externallyModified => (
                    <BooleanDecay value={externallyModified} delay={1000}>
                      {highlight => (
                        <TextInput1D
                          inputName="stowageFactor"
                          inputId="stowageFactor"
                          inputLabel="Stowage Factor"
                          inputPlaceholder="Enter Stowage Factor..."
                          unitChoices={sf}
                          selectedUnit={SFUnit}
                          setSelectedUnit={setSFUnit}
                        />
                      )}
                    </BooleanDecay>)}
                </ExternalModificationDetector>

                <TextInput
                  inputName="cargoTolerance"
                  inputId="cargoTolerance"
                  inputLabel="Cargo Tolerance (%age - MOLOO)"
                  inputPlaceholder="Enter cargo tolerance..."
                />

                <TextInput
                  inputName="seaMargin"
                  inputId="seaMargin"
                  inputLabel="Sea Margin (%age)"
                  inputPlaceholder="Enter sea margin..."
                />

                <TextInput
                  inputName="commission"
                  inputId="commission"
                  inputLabel="Commission (%age)"
                  inputPlaceholder="Enter commission..."
                />

                <Separator title="Load Port details" />
               <TextInputSuggestion
                  inputName="loadPort"
                  inputId="loadPort"
                  inputLabel="Load Port"
                  inputPlaceholder="Enter Load Port..."
                  token={userData.token}
                />

               {/* <TextInput1D
                  inputName="loadPortDraft"
                  inputId="loadPortDraft"
                  inputLabel="Load Port Draft"
                  inputPlaceholder="Enter Load Port Draft..."
                  unitChoices={drafts}
                  selectedUnit={loadPortDraft}
                  setSelectedUnit={setLoadPortDraft}
               /> */}

                <TextInput1Drop
                 inputName="loadPortDraft"
                 inputId="loadPortDraft"
                 inputLabel="Load Port Draft (M)"
                 inputPlaceholder="Enter Load Port Draft..."
                 unitChoices={drafts}
                 selectedUnit={loadPortDraft}
                 setSelectedUnit={setLoadPortDraft}
                 setSelectionTerm={setLoadPortDraftSelection}
                />  


            {/* <TextInput2D
                  inputName="loadType"
                  inputId="loadType"
                  inputLabel="Enter load details"
                  inputPlaceholder="Enter load details..."
                  unit1Choices={types}
                  unit2Choices={terms}
                  selectedUnit1={loadType}
                  selectedUnit2={loadTerm}
                  setSelectedUnit1={setLoadType}
                  setSelectedUnit2={setLoadTerm}
                />*/}    

             <TextInput2Drop
                 inputName="loadType"
                 inputId="loadType"
                 inputLabel="Load Terms"
                 inputPlaceholder="Enter load terms..."
                 unitChoices={port_terms}
                 unit1Choices={types}
                 selectedUnit1={loadType}
                 selectedUnit={loadTermDrop}
                 setSelectedUnit={setLoadTermDrop}
                 setSelectedUnit1={setLoadType}
                 setSelectionTerm={setLoadTermSelection}
                />   

                <TextInput
                  inputName="loadPortCost"
                  inputId="loadPortCost"
                  inputLabel="Load Port Cost ($)"
                  inputPlaceholder="Enter Load Port Cost..."
                />

                <Separator title="Discharge Port Details" />
                <TextInputSuggestion
                  inputName="dischargePort"
                  inputId="dischargePort"
                  inputLabel="Discharge Port"
                  inputPlaceholder="Enter Discharge Port..."
                  token={userData.token}
                />

               {/* <TextInput1D
                  inputName="dischargePortDraft"
                  inputId="dischargePortDraft"
                  inputLabel="Discharge Port Draft"
                  inputPlaceholder="Enter Discharge Port Draft..."
                  unitChoices={drafts}
                  selectedUnit={dischargePortDraft}
                  setSelectedUnit={setDischargePortDraft}
               /> */}

                <TextInput1Drop
                 inputName="dischargePortDraft"
                 inputId="dischargePortDraft"
                 inputLabel="Discharge Port Draft"
                 inputPlaceholder="Enter Discharge Port Draft..."
                 unitChoices={drafts}
                 selectedUnit={dischargePortDraft}
                 setSelectedUnit={setDischargePortDraft}
                 setSelectionTerm={setDischargePortDraftSelection}
                />  

                {/*<TextInput2D
                  inputName="dischargeType"
                  inputId="dischargeType"
                  inputLabel="Enter discharge details"
                  inputPlaceholder="Enter discharge details..."
                  unit1Choices={types}
                  unit2Choices={terms}
                  selectedUnit1={dischargeType}
                  selectedUnit2={dischargeTerm}
                  setSelectedUnit1={setDischargeType}
                  setSelectedUnit2={setDischargeTerm}
                /> */}

                <TextInput2Drop
                 inputName="dischargeType"
                 inputId="dischargeType"
                 inputLabel="Discharge Terms"
                 inputPlaceholder="Enter discharge terms..."
                 unitChoices={port_terms}
                 unit1Choices={types}
                 selectedUnit1={dischargeType}
                 selectedUnit={dischargeTermDrop}
                 setSelectedUnit={setDischargeTermDrop}
                 setSelectedUnit1={setDischargeType}
                 setSelectionTerm={setDischargeTermSelection}
                />   

                <TextInput
                  inputName="dischargePortCost"
                  inputId="dischargePortCost"
                  inputLabel="Discharge Port Cost ($)"
                  inputPlaceholder="Enter Discharge Port Cost..."
                />

                <Separator title="Bunkering Port Details" />
                <SelectInput
                inputName="bunkerPort"
                inputId="bunkerPort"
                inputLabel="Bunkering Port"
                inputPlaceholder="Select Bunkering Port..."
                options={bunker_ports}
              />

                <TextInput
                  inputName="costBunkerPort"
                  inputId="costBunkerPort"
                  inputLabel="Bunker Port Cost ($)"
                  inputPlaceholder="Enter Bunker Port Cost..."
                />

                <TextInput
                  inputName="bunkerDuration"
                  inputId="bunkerDuration"
                  inputLabel="Est. Bunkering duration (days)"
                  inputPlaceholder="Enter duration in days..."
                />

                <ExternalModificationDetector name="fuelRate">
                  {externallyModified => (
                    <BooleanDecay value={externallyModified} delay={1000}>
                      {highlight => (
                        <TextInput
                          inputName="fuelRate"
                          inputId="fuelRate"
                          inputLabel="VLSFO Price ($ / MT) "
                          inputPlaceholder="Enter VLSFO Price..."
                        />
                      )}
                    </BooleanDecay>)}
                </ExternalModificationDetector>

                <ExternalModificationDetector name="gasRate">
                  {externallyModified => (
                    <BooleanDecay value={externallyModified} delay={1000}>
                      {highlight => (
                        <TextInput
                          inputName="gasRate"
                          inputId="gasRate"
                          inputLabel="MGO Price ($ / MT) "
                          inputPlaceholder="Enter MGO Price..."
                        />
                      )}
                    </BooleanDecay>)}
                </ExternalModificationDetector>
                
                <Separator title="Benchmark Baltic Index Details" />
                <TextInput1Drop
                 inputName="benchMarkBalticIndex"
                 inputId="benchMarkBalticIndex"
                 inputLabel="Benchmark Baltic Index"
                 inputPlaceholder="Enter Benchmark Baltic Index Value..."
                 unitChoices={bbiChoices}
                 selectedUnit={bbiType}
                 setSelectedUnit={setBbiType}
                 setSelectionTerm={setBbiTypeSelection}
                />

              {/*  <TextInput
                inputName="benchMarkBalticIndex"
                inputId="benchMarkBalticIndex"
                inputLabel="Benchmark Baltic Index"
                inputPlaceholder="Enter Benchmark Baltic Index Value..."
               />  */}

                <TextInputSuggestion
                  inputName="ballastPort"
                  inputId="ballastPort"
                  inputLabel="Ballast Port"
                  inputPlaceholder="Enter Ballast Port..."
                  token={userData.token}
                />

              </div>

              <button
                type="submit"
               // disabled={submitting || pristine}
                className="submitButton"
              >
                Match Ships
            </button>
            </form>
          )}
      </Form>

      {loading ? <IdleModal/> : ''}
      

    </div>
  );
};

export default InputInterfacePrev;
