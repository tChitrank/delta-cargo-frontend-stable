import React from 'react';

const IdleModal = ({showModal, handleClose, handleLogout, remainingTime}) => {

    return (
        <div className={`Idlemodal__wrapper Idlemodal__wrapper--shown`}>
      <div className='Idlemodal__background--hightlight' />
      <div className={`Idlemodal Idlemodal--shown`}>
        
        <div className='Idlemodal__body'>
        <div className="loader"/>

        </div>
      </div>
    </div>
    )
}

export default IdleModal