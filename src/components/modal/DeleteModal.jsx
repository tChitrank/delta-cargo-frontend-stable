import React from 'react';

const DeleteModal = ({ showModal, handleClose, handleDelete, category }) => {

  return (
    <div className={`Deletemodal__wrapper ${showModal ? 'Deletemodal__wrapper--shown' : ''}`}>
      <div className='Deletemodal__background--hightlight' />
      <div className={`Deletemodal ${showModal ? 'Deletemodal--shown' : ''}`}>
        <div className='Deletemodal__header'>
          <h1>Delete {category} !</h1>
        </div>
        
        <div className='Deletemodal__body'>
          <p className='Deletemodal__content'>Do You Want To Delete this {category} ?</p>
          

          <div style={{ display: 'flex', alignContent: 'center', justifyContent: 'flex-end' }}>
              <button
                type='button' className='cancel_delete-button' onClick={handleClose}>
                Cancel</button>
              <button
                type='button' className='delete-button' onClick={handleDelete}>
                Delete</button>
          </div>

        </div>
      </div>
    </div>




  )
}

export default DeleteModal