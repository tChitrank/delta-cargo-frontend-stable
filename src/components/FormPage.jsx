import React from 'react';

import Header from './Header';
// import Login from './Login';
import InputInterface from './InputInterface';

function FormPage(props) {
  //console.log('props',props)
  return (
    <>
      <Header {...props}/>
      <InputInterface {...props}/>
    </>
  );
}

export default FormPage;
