import React, { useState } from 'react';
import deltaLogin from '../assets/deltaLogin.png'
import deltaLogo from '../assets/brand-logo.png'
import user from '../assets/user.png'
import password from '../assets/key.png'
import { Redirect } from 'react-router-dom'
import { toast } from 'react-toastify';
import {successOptions, errorOptions} from '../toastConfig'

import strings from '../assets/strings'

function Login(props) {
  const [isLoggedIn, updateIsLoggedIn] = useState(false)

  const onSubmit = (e) => {
   e.preventDefault()
    fetch(strings.api_url+`login/`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json',
      },
      body: JSON.stringify({
        "email": document.getElementById('username').value,
        "password": document.getElementById('pass').value
      })
    })
      .then(response => {
        if (!response || !response.ok) throw Error(response.status);
        return response.json();
      })
      .then(data => {
       // console.log("data:",data.data)
        localStorage.setItem('UserInfo', JSON.stringify(data.data))
        toast.success("Welcome Back!",successOptions)
        updateIsLoggedIn(true)
      })
      .catch(err => toast.error('The username password combination does not work. Please try again.', errorOptions));
  }

  if(isLoggedIn){
    return <Redirect to="/form"/>
  }
    return (
      <div className="limiter">
        <div className="container-login100" style={{ backgroundImage: `url(${deltaLogin})` }}>
          <div style={{ position: 'absolute', top: 30, left: 30 }}>
            <img src={deltaLogo} style={{ width: 120, height: 120 }} alt="" /> 
          </div>
          <div className="wrap-login100 p-t-30 p-b-50">
            <span className="login100-form-title p-b-41">
              Account Login
				</span>
            <form className="login100-form p-b-33 p-t-5" onSubmit={onSubmit}>

              <div className="wrap-input100" style={{ display: 'flex' }}>
                <img src={user} style={{ width: 25, height: 25, marginLeft: 20, marginTop: 10 }} alt="" />
                <input className="input100" type="text" id="username" style={{ border: 'none', outline: 'none' }}
                  placeholder="Email"  autoComplete="off" />
                <span className="focus-input100"></span>
              </div>

              <div className="wrap-input100" style={{ display: 'flex' }}>
                <img src={password} style={{ width: 25, height: 25, marginLeft: 20, marginTop: 10 }} alt="" />
                <input className="input100" type="password" id="pass" placeholder="Password"
                  style={{ border: 'none', outline: 'none' }}  />
                <span className="focus-input100"></span>
              </div>

              <div className="container-login100-form-btn m-t-32">
                <button type="submit" className="login100-form-btn" style={{ fontFamily: 'Mulish', fontWeight: 'bold' }}>
                  Login
						</button>
              </div>

            </form>
          </div>
        </div>
      </div>
    )
}

export default Login;
