import React, { useState, useEffect } from 'react';
import Header from './Header';
import { toast } from 'react-toastify';

import UserUploader from './form-components/UserUploader'
import DeleteModal from './modal/DeleteModal'
import strings from '../assets/strings'

function Admin(props) {
    //let dummyUser = ['User1', 'User2', 'User3']
    let [userDetails, setUserDetails] = useState([])
    let [addUserVisible, setAddUserVisible] = useState(false)
    let [editUserVisible, setEditUserVisible] = useState(false)

    let [id, updateId] = useState("")
    let [fname, updateFname] = useState("")
    let [lname, updateLname] = useState("")
    let [email, updateEmail] = useState("")
    let [admin, updateAdmin] = useState(false)
    let [handy, updateHandy] = useState(false)
    let [supra, updateSupra] = useState(false)
    let [panamax, updatePanamax] = useState(false)
    let [idDelete,setIdDelete] = useState("")
    let [modalVisibility,setModalVisiblity] = useState(false)

    const userData = JSON.parse(localStorage.getItem('UserInfo'))

    useEffect(() => {
        fetch(strings.api_url + `get_user_data/`, {
            method: 'GET',
            headers: {
                DELTAAUTH: userData.token,
                'Accept': 'application/json',
                'Content-type': 'application/json',
            },
        })
            .then(response => {
                if (!response || !response.ok) throw alert("Chitrank");
                return response.json();
            })
            .then(data => {
                setUserDetails(data.data)
            })
            .catch(err => console.log('Error Retreiving', err));
    }, []);

    const toggleAdminState = () => {
        updateAdmin((prevState) => !prevState);
    }
    const toggleHandyState = () => {
        updateHandy((prevState) => !prevState);
    }
    const toggleSupraState = () => {
        updateSupra((prevState) => !prevState);
    }
    const togglePanamaxState = () => {
        updatePanamax((prevState) => !prevState);
    }

    const addUserButton = () => {
        setAddUserVisible(true)
        setEditUserVisible(false)
    }
    const editUserButton = (obj) => {
        updateId(obj.id)
        updateFname(obj.first_name)
        updateLname(obj.last_name)
        updateEmail(obj.email)
        updateAdmin(obj.admin)
        updateHandy(obj.handy)
        updateSupra(obj.supra)
        updatePanamax(obj.panamax)
        setAddUserVisible(false)
        setEditUserVisible(true)
    }

    const handleClose=()=> {
        setModalVisiblity(false)
    }
    const handleDelete=()=> {
        console.log("user deleted")
        const url = strings.api_url + `delete_user/`;
        const requestConfig = {
            method: "POST",
            headers: {
                DELTAAUTH: userData.token,
                'Accept': 'application/json',
                'Content-type': 'application/json',
            },
            body: JSON.stringify({
                id : idDelete
            }),
        };
        fetch(url, requestConfig)
        .then(response => {
            if (response && response.ok) return response.json();
            throw new Error("failed to delete user");
        })
        .then(data => {
            //console.log('data',data)
            window.location.reload()
            setModalVisiblity(false)
        })
        .catch(err => {
            console.log(err);
            toast.error("Could not delete the user, please try again!");
        });
    }

    const userRawData = userDetails.map(obj => {
        return (
            <div style={{
                backgroundColor: '#eaeaea', height: 38,
                margin: 5, display: 'flex', marginBottom: 5, marginTop: 5
            }}>
                <p style={{ margin: 5, fontSize: 12, width: '90%' }}>{obj.email}</p>
                <div style={{ display: 'flex', justifyItems: 'flex-end' }}>
                    <button
                        type="button"
                        className="edit_user-button"
                        onClick={() => editUserButton(obj)}
                    >
                        Edit
                    </button>
                    <button
                        type="button"
                        className="delete_user-button"
                        onClick={()=> {setModalVisiblity(true)
                                        setIdDelete(obj.id)}}
                    >
                        Delete
                    </button>
                </div>
            </div>
        )
    })

    return (
        <>
            <Header {...props} />

            <div style={{ width: '95%', display: 'flex', justifyContent: 'flex-end', margin: 15 }}>
                <button
                    type="button"
                    className="adduser-button"
                    onClick={addUserButton}
                >
                    Add User
                    </button>
            </div>

            <div style={{ display: 'flex' }}>
                <div className="admin__user-list custom-scrollbar" style={{ width: '50%', marginLeft: '8px' }}>
                    {userRawData}
                </div>

                {addUserVisible ?
                    <div style={{ marginLeft: 4, width: '50%' }}>
                        <UserUploader type="Create User" />
                    </div>
                    : ''}

                {editUserVisible ?
                    <div style={{ marginLeft: 4, width: '50%' }}>
                        <UserUploader type="Edit User"
                            id={id} fname={fname} lname={lname} email={email}
                            admin={admin} handy={handy} supra={supra} panamax={panamax}
                            onAdminChangeHandler={toggleAdminState}
                            onHandyChangeHandler={toggleHandyState}
                            onSupraChangeHandler={toggleSupraState}
                            onPanamaxChangeHandler={togglePanamaxState}
                        />
                    </div>
                    : ''}

                <DeleteModal
                    category={"User"}
                    showModal={modalVisibility}
                    handleClose={handleClose}
                    handleDelete={handleDelete} />
            </div>
        </>
    )
}

export default Admin