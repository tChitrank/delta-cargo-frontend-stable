import React, { useState,useEffect } from 'react';
import Header from './Header';
import strings from '../assets/strings'
import downloadReport from '../assets/excel.png'
import CsvDownload from 'react-json-to-csv'

function ShowPrevious(props) {
   // let formData = JSON.parse(localStorage.getItem('formData'))

  //  const [data, updateData] = useState(strings.data)
  //  const [reqData, updateReqData] = useState(formData)
    const [previousData,updatePreviousData] = useState([])
    const userData = JSON.parse(localStorage.getItem('UserInfo'))
    const segmentArray = JSON.parse(localStorage.getItem('userSegmentData'))
    console.log('segment',segmentArray)

    useEffect(()=>{
        fetch(strings.api_url+`api/show_recent_log/`, {
            method: 'POST',
            headers: {
               DELTAAUTH: userData.token,
              'Accept': 'application/json',
              'Content-type': 'application/json',
            },
            body: JSON.stringify({
                "vessel_segment": segmentArray
              })
          })
            .then(response => {
              if (!response || !response.ok) throw Error(response.status);
              return response.json();
            })
            .then(data => {
             // console.log("data:",data.data)
            updatePreviousData(data.data)
            })
            .catch(err => console.log('Error Retreiving', err));
    },[])

    const handleShowResults=(responseData,requestData)=>{
        console.log(responseData.data)
        console.log(requestData)
        localStorage.setItem('responseData',JSON.stringify(responseData.data))
        localStorage.setItem('requestData',JSON.stringify(requestData))
        props.history.push('/results') 
    }

    const previousDataDiv = previousData.map(obj => {
            return (
                <div style={{
                    backgroundColor: '#eaeaea', height: 64,
                    margin: 20, display: 'flex', marginBottom: 16, marginTop: 16, justifyContent: 'space-evenly'
                }}>
            <p style={{marginTop:10,position:'absolute',left:'3%',fontSize:16}}>{obj.created_at}</p>
            <p style={{marginTop:10,position:'absolute',left:'26%',fontSize:16}}>{obj.request_data.vessel_segment}</p>
            <p style={{marginTop:10,position:'absolute',left:'40%',fontSize:16}}>{obj.request_data.vessel_loading_zone}</p>
                   
                    <div style={{ display:'flex',position:'absolute',left:'54%' }}>
                        <img src={downloadReport} alt=""
                            style={{ width: 44, height: 44,margin:10 }} />
                        <CsvDownload
                            className="downloadReport"
                            data={Object.entries(obj.request_data)} 
                            filename={`form`+obj.created_at+`.csv`}
                            style={{
                                border: 'none',
                                outline: 'none',
                                textAlign: 'center',
                                textDecoration: 'none',
                                display: 'inline-block',
                                cursor: 'pointer'
                            }}
                        ><p style={{ fontSize: 16 , marginTop:20}}>
                        Form({obj.created_at})</p>
                        </CsvDownload>
                    </div>

                    <button  style={{marginTop:10,position:'absolute',
            left:'82%'}}
            onClick={()=>{ handleShowResults(obj.response_data,obj.request_data) }}
                type="submit"
                className="submitButton"
              >
                Show Results
            </button>
            
        {/* <p className="downloadReport"
            style={{marginTop:10,position:'absolute',
            right:100,fontSize:16,
            border: 'none',
            outline: 'none',
            textAlign: 'center',
            textDecoration: 'none',
            display: 'inline-block',
            fontWeight:'bold',
            color:'#d32f2f',
            cursor: 'pointer'}}>Show Results</p>*/}    
                   

                </div>
            )
        })
    return (
        <>
            <Header {...props}/>

            <p style={{fontSize:24,margin:20,marginTop:8,marginBottom:8,fontWeight:'bold'}}>Get Previous Requests/Responses</p>

            <div style={{backgroundColor:'#202020', height:84,
            margin:20,marginTop:8,display:'flex',marginBottom:25}}>
                <p style={{color:'white',fontWeight:'bold',fontSize:16,textAlign:'center',position:'absolute',top:165,left:'5%'}}>Date</p>
                <p style={{color:'white',fontWeight:'bold',fontSize:16,textAlign:'center',position:'absolute',top:165,left:'23%'}}>Vessel Segment</p>
                <p style={{color:'white',fontWeight:'bold',fontSize:16,textAlign:'center',position:'absolute',top:165,left:'38%'}}>Vessel Loading Zone</p>
                <p style={{color:'white',fontWeight:'bold',fontSize:16,textAlign:'center',position:'absolute',top:165,left:'58%'}}>Request Details PDF</p>
                <p style={{color:'white',fontWeight:'bold',fontSize:16,textAlign:'center',position:'absolute',top:165,left:'86%'}}>Results </p>
               
            </div>

            {previousDataDiv}

        </>
    )
}

export default ShowPrevious