import React from 'react';
import Routing from '../route'
import IdleModalComponent from './IdleModalComponent'

class Layout extends React.Component {
  constructor(props){
    super(props)
    }

  render(){
  return (
      <React.Fragment >
        
    <IdleModalComponent {...this.props}/>
          <Routing/>

      </React.Fragment>
  );
  }
}

export default Layout;
