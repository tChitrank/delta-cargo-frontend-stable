import React from 'react'
import { Redirect } from 'react-router-dom'
import { toast } from 'react-toastify';
import {successOptions} from '../toastConfig'
export default class Logout extends React.Component {

    constructor(props) {
        super(props)
        localStorage.removeItem('UserInfo')
    }

    render() {
        toast.success("User is Logged Out.",successOptions)
        return (
           <Redirect to='/'/>
        )
    }
}
