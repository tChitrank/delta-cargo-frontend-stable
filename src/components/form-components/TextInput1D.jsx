import React from 'react';
import { Field } from 'react-final-form';
import PropTypes from 'prop-types';

const TextInput1D = ({
  inputName, inputId, inputLabel, inputPlaceholder, unitChoices, selectedUnit, setSelectedUnit
}) => (
  <div className="formControl">
    <label htmlFor={inputId} className="inputLabel">{inputLabel}</label>
    <div className="formControl__inputWrapper">
      <Field
        name={inputName}
        id={inputId}
        placeholder={inputPlaceholder}
      >
       {({ id, placeholder, input: { name, value , onChange },meta }) => (
           <div style={{width:'25rem',wordBreak:"break-all"}}>
          <input
            type="text"
            name={name}
            id={id}
            value={value}
            onChange={onChange}
            placeholder={placeholder}
            className="inputField inputField1D"
          />
           {meta.error && meta.touched && <p style={{color:'red'}}>{meta.error}</p>}
          </div>
        ) }
      </Field>
      <select
        className="inputField1D__dropdown"
        value={selectedUnit}
        onChange={(event) => setSelectedUnit(event.target.value)}
      >
        {unitChoices.map((choice) => (
          <option key={choice} value={choice} title={choice}>{choice}</option>
        ))}
      </select>
    </div>
  </div>
);

TextInput1D.propTypes = {
  inputName: PropTypes.string.isRequired,
  inputId: PropTypes.string.isRequired,
  inputLabel: PropTypes.string.isRequired,
  inputPlaceholder: PropTypes.string.isRequired,
  unitChoices: PropTypes.arrayOf(PropTypes.string).isRequired,
  selectedUnit: PropTypes.string.isRequired,
  setSelectedUnit: PropTypes.func.isRequired,
};

export default TextInput1D;
