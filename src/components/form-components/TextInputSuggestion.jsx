import React,{useState} from 'react';
import { Field } from 'react-final-form';
import PropTypes from 'prop-types';
import Autosuggest from 'react-autosuggest';
import strings from '../../assets/strings'

const TextInputSuggestion = ({
  inputName, inputId, inputLabel, inputPlaceholder, inputValue , token
}) => {

    const [suggestions, setSuggestions] = useState([])

    return(
  <div className="formControl">
    <label htmlFor={inputId} className="inputLabel">{inputLabel}</label>
    <div className="formControl__inputWrapper">
      <Field
        name={inputName}
        id={inputId}
        placeholder={inputPlaceholder}
        value={inputValue}
      >
        {({ id, placeholder, input: { name, value, onChange },meta}) => (
          <div style={{width:'40rem',wordBreak:"break-all"}} 
          className="autoSuggestClass">
         <Autosuggest
         className='inputField'
        inputProps={{
          placeholder: placeholder,
          autoComplete: "off",
          name: name,
          id: id,
          value: value,
          onChange: onChange
        }}
        suggestions={suggestions}
        onSuggestionsFetchRequested={async ({ value }) => {
       // console.log(value,'suggestion')
          if (!value) {
            setSuggestions([]);
            return;
          }
          try {
            const url = strings.api_url+`api/show_ports/?name=${value}`;
            
             // console.log('token',token)
             // console.log('values',value)
            fetch(url, {
              method: 'GET',
              headers: {
                DELTAAUTH: token,
                'Accept': 'application/json',
                'Content-type': 'application/json',
              },
            })
              .then(response => {
                if (!response || !response.ok) throw Error(response.status);
                return response.json()
              })
              .then(data => {
               // console.log(data.data, 'suggestion name')
                setSuggestions(data.data.map(row => ({ name: row.ports })))

              })
              .catch(err => console.log('Could not get suggestion name', err));

          }
          catch (e) {
            setSuggestions([]);
          }
        }
      }
        onSuggestionsClearRequested={() => {
          setSuggestions([]);
        }}
        onSuggestionSelected={(_event, { suggestion, method }) => {
          if (method === "enter" || method === 'click') {
           // console.log('suggestion name', suggestion.name)
            onChange(suggestion.name)
          }
        }}
        getSuggestionValue={suggestion => suggestion.name}
        renderSuggestion={suggestion => <div>{suggestion.name}</div>}


      />
      {/*  <input
            type="text"
            name={name}
            id={id}
            value={value}
            onChange={onChange}
            placeholder={placeholder}
            className="inputField"
          />
          {meta.error && meta.touched && <p style={{color:'red'}}>{meta.error}</p>}*/}   
          </div>
        )}
      </Field>
    </div>
  </div>
)}

TextInputSuggestion.propTypes = {
  inputName: PropTypes.string.isRequired,
  inputId: PropTypes.string.isRequired,
  inputLabel: PropTypes.string.isRequired,
  inputPlaceholder: PropTypes.string.isRequired,
};

export default TextInputSuggestion;
