import React from 'react';
import { Field } from 'react-final-form';
import PropTypes from 'prop-types';
import { Dropdown } from 'office-ui-fabric-react';

const onRenderOption = (item) => {
  //  console.log("choice",item)
    return (
      <table>
        <tbody>
          <tr>
    <td style={{ width: 60 }}>{item.text}</td>
            <td style={{ width: 50 }} />
            <td >
              {item.key}
            </td>
          </tr>
        </tbody>
      </table>
    );
  };

const TextInput1Drop = ({
  inputName, inputId, inputLabel, inputPlaceholder, unitChoices, selectedUnit, setSelectedUnit , setSelectionTerm,
}) => (
  <div className="formControl">
    <label htmlFor={inputId} className="inputLabel">{inputLabel}</label>
    <div className="formControl__inputWrapper">
      <Field
        name={inputName}
        id={inputId}
        placeholder={inputPlaceholder}
      >
       {({ id, placeholder, input: { name, value , onChange },meta }) => (
           <div style={{width:'25rem',wordBreak:"break-all"}}>
          <input
            type="text"
            name={name}
            id={id}
            value={value}
            onChange={onChange}
            placeholder={placeholder}
            className="inputField inputField1D"
          />
           {meta.error && meta.touched && <p style={{color:'red'}}>{meta.error}</p>}
          </div>
        ) }
      </Field>
     
    <Dropdown
      className="inputField1D__dropdown"
      placeholder="Baltic Index"
      options={unitChoices}
      selectedKey={selectedUnit}
      dropdownWidth={350}
      onChange={(event,selectedOption) => { //console.log("event",selectedOption)
          setSelectedUnit(selectedOption.key)
          setSelectionTerm(selectedOption.text)
          }}
      onRenderOption={onRenderOption}
    />
 
     
    </div>
  </div>
);

TextInput1Drop.propTypes = {
  inputName: PropTypes.string.isRequired,
  inputId: PropTypes.string.isRequired,
  inputLabel: PropTypes.string.isRequired,
  inputPlaceholder: PropTypes.string.isRequired,
  unitChoices: PropTypes.arrayOf(PropTypes.string).isRequired,
  selectedUnit: PropTypes.string.isRequired,
  setSelectedUnit: PropTypes.func.isRequired,
  setSelectionTerm: PropTypes.func.isRequired,
};

export default TextInput1Drop;
