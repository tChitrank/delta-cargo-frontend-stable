import React from 'react';
import { Field } from 'react-final-form';
import PropTypes from 'prop-types';

const TextInput2D = ({
  inputName,
  inputId,
  inputLabel,
  inputPlaceholder,
  unit1Choices,
  unit2Choices,
  selectedUnit1,
  selectedUnit2,
  setSelectedUnit1,
  setSelectedUnit2,
}) => (
  <div className="formControl">
    <label htmlFor={inputId} className="inputLabel">{inputLabel}</label>
    <div className="formControl__inputWrapper">
      <Field
        name={inputName}
        id={inputId}
        placeholder={inputPlaceholder}
      >
        {({ id, placeholder, input: { name, value, onChange }, meta }) => (
          <div style={{width:'15rem',wordBreak:"break-all"}}>
          <input
            type="text"
            name={name}
            id={id}
            value={value}
            onChange={onChange}
            placeholder={placeholder}
            className="inputField inputField2D"
          />
           {meta.error && meta.touched && <p style={{color:'red'}}>{meta.error}</p>}
          </div>
        )}
      </Field>
      <select
        className="inputField2D__dropdown1"
        value={selectedUnit1}
        onChange={(event) => setSelectedUnit1(event.target.value)}
      >
        {unit1Choices.map((choice) => (
          <option key={choice} value={choice}>{choice}</option>
        ))}
      </select>
      <select
        className="inputField2D__dropdown2"
        value={selectedUnit2}
        onChange={(event) => setSelectedUnit2(event.target.value)}
      >
        {unit2Choices.map((choice) => (
          <option key={choice} value={choice}>{choice}</option>
        ))}
      </select>
    </div>
  </div>
);

TextInput2D.propTypes = {
  inputName: PropTypes.string.isRequired,
  inputId: PropTypes.string.isRequired,
  inputLabel: PropTypes.string.isRequired,
  inputPlaceholder: PropTypes.string.isRequired,
  unit1Choices: PropTypes.arrayOf(PropTypes.string).isRequired,
  unit2Choices: PropTypes.arrayOf(PropTypes.string).isRequired,
  selectedUnit1: PropTypes.string.isRequired,
  selectedUnit2: PropTypes.string.isRequired,
  setSelectedUnit1: PropTypes.func.isRequired,
  setSelectedUnit2: PropTypes.func.isRequired,
};

export default TextInput2D;
