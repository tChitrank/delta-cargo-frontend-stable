import React from 'react';
import { Field } from 'react-final-form';
import PropTypes from 'prop-types';

const TextInput = ({
  inputName, inputId, inputLabel, inputPlaceholder, inputValue, isDisabled
}) => (
  <div className="formControl">
    <label htmlFor={inputId} className="inputLabel">{inputLabel}</label>
    <div className="formControl__inputWrapper">
      <Field
        name={inputName}
        id={inputId}
        placeholder={inputPlaceholder}
        value={inputValue}
      >
        
        {({ id, placeholder, input: { name, value, onChange },meta}) => (
          <div style={{width:'40rem',wordBreak:"break-all"}}>
          <input
            type="text"
            name={name}
            disabled={isDisabled}
            id={id}
            value={value}
            onChange={onChange}
            placeholder={placeholder}
            className="inputField"
          />
          {meta.error && meta.touched && <p style={{color:'red'}}>{meta.error}</p>}
          </div>
        )}
      </Field>
    </div>
  </div>
);

TextInput.propTypes = {
  inputName: PropTypes.string.isRequired,
  inputId: PropTypes.string.isRequired,
  inputLabel: PropTypes.string.isRequired,
  inputPlaceholder: PropTypes.string.isRequired,
};

export default TextInput;
