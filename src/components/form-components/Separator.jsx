import React from 'react';
import PropTypes from 'prop-types';

const Separator = ({ title }) => (
  <div className="separator">
    <span className="separator__lineBefore" />
    <p className="separator__text">{title}</p>
    <span className="separator__lineAfter" />
  </div>
);

Separator.propTypes = {
  title: PropTypes.string.isRequired,
};

export default Separator;
