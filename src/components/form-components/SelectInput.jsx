import React from 'react';
import { Field } from 'react-final-form';
import Select from 'react-select';
import PropTypes from 'prop-types';

const customStyles = {
  control: (styles) => ({ ...styles, maxWidth: '35rem',fontSize:12,fontWeight:'12rem',fontColor:'black'}),
};

const SelectInput = ({
  inputName, inputId, inputLabel, inputPlaceholder, options, multipleSelection, searchable
}) => (
  <div className="formControl">
    <label htmlFor={inputId} className="inputLabel">{inputLabel}</label>
    <Field
      name={inputName}
      id={inputId}
      placeholder={inputPlaceholder}
      options={options}
    > 
      {({ input, meta, ...rest}) => (
        // eslint-disable-next-line
        <div style={{width:'40rem',wordBreak:"break-all"}}>
        <Select {...input} {...rest} 
        styles={customStyles} 
        isMulti={multipleSelection}
        isSearchable={searchable}/>

        {meta.error && meta.touched && <p style={{color:'red'}}>{meta.error}</p>}
        </div>
      )}
    </Field>
  </div>
  );

SelectInput.propTypes = {
  inputName: PropTypes.string.isRequired,
  inputId: PropTypes.string.isRequired,
  inputLabel: PropTypes.string.isRequired,
  inputPlaceholder: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default SelectInput;
