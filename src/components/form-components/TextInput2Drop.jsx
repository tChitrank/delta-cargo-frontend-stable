import React from 'react';
import { Field } from 'react-final-form';
import PropTypes from 'prop-types';
import { Dropdown} from 'office-ui-fabric-react';

const onRenderOption = (item) => {
  //  console.log("choice",item)
    return (
      <table>
        <tbody>
          <tr>
    <td style={{ width: 40 }}>{item.text}</td>
            <td style={{ width: 50 }} />
            <td >
              {item.key}
            </td>
          </tr>
        </tbody>
      </table>
    );
  };

const TextInput2Drop = ({
  inputName, inputId, inputLabel, inputPlaceholder, unitChoices, selectedUnit, setSelectedUnit , setSelectionTerm,
  selectedUnit1,
  setSelectedUnit1,unit1Choices,
}) => (
  <div className="formControl">
    <label htmlFor={inputId} className="inputLabel">{inputLabel}</label>
    <div className="formControl__inputWrapper">
      <Field
        name={inputName}
        id={inputId}
        placeholder={inputPlaceholder}
      >
       {({ id, placeholder, input: { name, value , onChange },meta }) => (
           <div style={{width:'15rem',wordBreak:"break-all"}}>
          <input
            type="text"
            name={name}
            id={id}
            value={value}
            onChange={onChange}
            placeholder={placeholder}
            className="inputField inputField2D"
          />
           {meta.error && meta.touched && <p style={{color:'red'}}>{meta.error}</p>}
          </div>
        ) }
      </Field>
      <select
        className="inputField2D__dropdown1"
        value={selectedUnit1}
        onChange={(event) => setSelectedUnit1(event.target.value)}
      >
        {unit1Choices.map((choice) => (
          <option key={choice} value={choice}>{choice}</option>
        ))}
      </select>
     
    <Dropdown
      className="inputField2D__dropdown2"
      placeholder="Port Terms"
      options={unitChoices}
      selectedKey={selectedUnit}
      dropdownWidth={350}
      onChange={(event,selectedOption) => { //console.log("event",selectedOption)
          setSelectedUnit(selectedOption.key)
          setSelectionTerm(selectedOption.text)
          }}
      onRenderOption={onRenderOption}
    />
 
     
    </div>
  </div>
);

TextInput2Drop.propTypes = {
  inputName: PropTypes.string.isRequired,
  inputId: PropTypes.string.isRequired,
  inputLabel: PropTypes.string.isRequired,
  inputPlaceholder: PropTypes.string.isRequired,
  unit1Choices: PropTypes.arrayOf(PropTypes.string).isRequired,
  unitChoices: PropTypes.arrayOf(PropTypes.string).isRequired,
  selectedUnit: PropTypes.string.isRequired,
  setSelectedUnit: PropTypes.func.isRequired,
  setSelectionTerm: PropTypes.func.isRequired,
  selectedUnit1: PropTypes.string.isRequired,
  setSelectedUnit1: PropTypes.func.isRequired,
};

export default TextInput2Drop;
