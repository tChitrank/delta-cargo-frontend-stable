import React from "react";
import { toast } from 'react-toastify';

import { successOptions } from '../../toastConfig'
import strings from '../../assets/strings'

class UserUploader extends React.Component {
    state = {
        fname: "",
        lname: "",
        email: "",
        password: "",
        admin: false,
        handy: false,
        supra: false,
        panamax: false,
        loading: false,
    };

    onFnameChange = (e) => {
        this.setState({ fname: e.target.value });
    };
    onLnameChange = (e) => {
        this.setState({ lname: e.target.value });
    };
    onEmailChange = (e) => {
        this.setState({ email: e.target.value });
    };
    onPasswordChange = (e) => {
        this.setState({ password: e.target.value });
    };

    userData = JSON.parse(localStorage.getItem('UserInfo'))

    createUser = (e) => {
        e.preventDefault()

            fetch(strings.api_url + `signup/`, {
                method: 'POST',
                headers: {
                    DELTAAUTH: this.userData.token,
                    'Accept': 'application/json',
                    'Content-type': 'application/json',
                },
                body: JSON.stringify({
                    "first_name": this.state.fname,
                    "last_name": this.state.lname,
                    "email": this.state.email,
                    "password": this.state.password,
                    "admin": this.state.admin,
                    "supra": this.state.supra,
                    "panamax": this.state.panamax,
                    "handy": this.state.handy
                })
            })
                .then(response => {
                    if (!response || !response.ok) throw alert("Check Internet Connection");
                    return response.json();
                })
                .then(data => {
                    // console.log("data:",data.data)
                    toast.success("User Successfully Entered!", successOptions)
                    window.location.reload()
                    //alert("Successfully Entered")
                })
                .catch(err => console.log('Error Retreiving', err));
    }

    updateUser=(e)=>{
        e.preventDefault()

        let updatedUserData={}
        updatedUserData = {
            id:this.props.id,
            first_name:this.state.fname,
            last_name:this.state.lname,
            password:this.state.password,
            email:this.state.email,
            admin:this.props.admin,
            handy:this.props.handy,
            supra:this.props.supra,
            panamax:this.props.panamax
        }
        for (let [key, value] of Object.entries(updatedUserData)) {
            if(value===null || value==="" ){
               delete updatedUserData[key]
            }
        }
        console.log(updatedUserData)
        fetch(strings.api_url + `edit_user/`, {
            method: 'POST',
            headers: {
                DELTAAUTH: this.userData.token,
                'Accept': 'application/json',
                'Content-type': 'application/json',
            },
            body: JSON.stringify(updatedUserData)
        })
            .then(response => {
                if (!response || !response.ok) throw alert("Check Internet Connection");
                return response.json();
            })
            .then(data => {
                // console.log("data:",data.data)
                toast.success("User Successfully Updated!", successOptions)
                window.location.reload()
                //alert("Successfully Entered")
            })
            .catch(err => console.log('Error Retreiving', err));
    }

    render() {
        if (this.props.type === "Create User") {
            return (
                <div className="upload-wrapper" style={{ marginRight: '8px' }}>
                    <h3 className="upload-title">Create User</h3>

                    <label className="upload-label">First Name</label>
                    <input
                        type="text"
                        name="f_name"
                        style={{ width: '80%', padding: '4px' }}
                        placeholder="Enter Employee First Name"
                        onChange={this.onFnameChange}
                    />

                    <label className="upload-label">Last Name</label>
                    <input
                        type="text"
                        name="l_name"
                        style={{ width: '80%', padding: '4px' }}
                        placeholder="Enter Employee Last Name"
                        onChange={this.onLnameChange}
                    />

                    <label className="upload-label">Email</label>
                    <input
                        type="email"
                        name="email"
                        style={{ width: '80%', padding: '4px' }}
                        placeholder="Enter Employee Email"
                        onChange={this.onEmailChange}
                    />

                    <label className="upload-label">Password</label>
                    <input
                        type="text"
                        name="password"
                        style={{ width: '80%', padding: '4px' }}
                        placeholder="Enter Employee Password"
                        onChange={this.onPasswordChange}
                    />

                    <div style={{ display: 'flex' }}>
                        <label className="upload-label" style={{ marginTop: 10 }}>Admin Access</label>
                        <input
                            type="checkbox"
                            name="admin"
                            className="upload-input"
                            style={{ margin: 10 }}
                            onChange={() => this.setState({ admin: !this.state.admin })}
                        />
                    </div>

                    <div style={{ display: 'flex' }}>
                        <label className="upload-label" style={{ marginTop: 10 }}>Handy Access</label>
                        <input
                            type="checkbox"
                            name="handy"
                            className="upload-input"
                            style={{ margin: 10 }}
                            onChange={() => this.setState({ handy: !this.state.handy })}
                        />
                    </div>

                    <div style={{ display: 'flex' }}>
                        <label className="upload-label" style={{ marginTop: 10 }}>Supra Access</label>
                        <input
                            type="checkbox"
                            name="supra"
                            className="upload-input"
                            style={{ margin: 10 }}
                            onChange={() => this.setState({ supra: !this.state.supra })}
                        />
                    </div>

                    <div style={{ display: 'flex' }}>
                        <label className="upload-label" style={{ marginTop: 10 }}>Panamax Access</label>
                        <input
                            type="checkbox"
                            name="panamax"
                            className="upload-input"
                            style={{ margin: 10 }}
                            onChange={() => this.setState({ panamax: !this.state.panamax })}
                        />
                    </div>

                    <button
                        type="button"
                        className="create-user-button"
                    onClick={this.createUser}
                    >
                        Submit
                </button>

                </div>
            )
        }
        else {
            return (
                <div className="upload-wrapper" style={{ marginRight: '8px' }}>
                    <h3 className="upload-title">Edit User</h3>

                    <label className="upload-label"> <strong>Employee Firstname : </strong>{this.props.fname}</label>
                    <label className="upload-label"><strong>Employee Lastname : </strong>{this.props.lname}</label>
                    <label className="upload-label"><strong>Employee Email : </strong>{this.props.email}</label>
                    <label className="upload-label"><strong>Admin Access : </strong>{JSON.stringify(this.props.admin)}</label>
                    <div style={{display: 'flex'}}>
                    <label className="upload-label" style={{marginRight:'12px'}}><strong>Handy Access : </strong>{JSON.stringify(this.props.handy)}</label>
                    <label className="upload-label" style={{marginRight:'12px'}}><strong>Supra Access : </strong>{JSON.stringify(this.props.supra)}</label>
                    <label className="upload-label" style={{marginRight:'12px'}}><strong>Panamax Access : </strong>{JSON.stringify(this.props.panamax)}</label>
                    </div>

                    <label className="upload-label">First Name</label>
                    <input
                        type="text"
                        name="f_name"
                        style={{ width: '80%', padding: '4px' }}
                        placeholder="Update Employee First Name"
                        onChange={this.onFnameChange}
                    />

                    <label className="upload-label">Last Name</label>
                    <input
                        type="text"
                        name="l_name"
                        style={{ width: '80%', padding: '4px' }}
                        placeholder="Update Employee Last Name"
                        onChange={this.onLnameChange}
                    />

                    <label className="upload-label">Email</label>
                    <input
                        type="email"
                        name="email"
                        style={{ width: '80%', padding: '4px' }}
                        placeholder="Update Employee Email"
                        onChange={this.onEmailChange}
                    />

                    <label className="upload-label">Password</label>
                    <input
                        type="text"
                        name="password"
                        style={{ width: '80%', padding: '4px' }}
                        placeholder="Update Employee Password"
                        onChange={this.onPasswordChange}
                    />

                    <div style={{ display: 'flex' }}>
                        <label className="upload-label" style={{ marginTop: 10 }}>Admin Access</label>
                        <input
                            type="checkbox"
                            checked={this.props.admin}
                            name="admin"
                            className="upload-input"
                            style={{ margin: 10 }}
                            onChange={this.props.onAdminChangeHandler}
                        />
                    </div>

                    <div style={{display: 'flex'}}>
                    <div style={{ display: 'flex' }}>
                        <label className="upload-label" style={{ marginTop: 10 }}>Handy Access</label>
                        <input
                            type="checkbox"
                            checked={this.props.handy}
                            name="handy"
                            className="upload-input"
                            style={{ margin: 10 }}
                            onChange={this.props.onHandyChangeHandler}
                        />
                    </div>

                    <div style={{ display: 'flex' }}>
                        <label className="upload-label" style={{ marginTop: 10 }}>Supra Access</label>
                        <input
                            type="checkbox"
                            checked={this.props.supra}
                            name="supra"
                            className="upload-input"
                            style={{ margin: 10 }}
                            onChange={this.props.onSupraChangeHandler}
                        />
                    </div>

                    <div style={{ display: 'flex' }}>
                        <label className="upload-label" style={{ marginTop: 10 }}>Panamax Access</label>
                        <input
                            type="checkbox"
                            checked={this.props.panamax}
                            name="panamax"
                            className="upload-input"
                            style={{ margin: 10 }}
                            onChange={this.props.onPanamaxChangeHandler}
                        />
                    </div>
                    </div>

                    <button
                        type="button"
                        className="create-user-button"
                    onClick={this.updateUser}
                    >
                        Submit
                </button>

                </div>
            )
        }
    }
}

export default UserUploader