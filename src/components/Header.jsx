import React, { useState } from "react";
import { Link } from "react-router-dom";
import deltaLogo from "../assets/brand-logo.png";
import expandIcon from "../assets/expand.svg";

const Header = (props) => {
  let [showUserDropdown, updateShowUserDropdown] = useState(false);
  let userInfo = JSON.parse(localStorage.getItem('UserInfo'))

  let toggleUserDropdown = () => {
    updateShowUserDropdown(!showUserDropdown)
}
  
let handleAdminPage = () =>{
  props.history.push('/admin')
}
let handleChangePasswordPage = () =>{
  props.history.push('/changePassword')
}
let handleNewCargo = () =>{
  props.history.push('/form')
}
let handlePreviousCargo = () =>{
  props.history.push('/formPrev')
}
  let handlePreviousResults = () =>{
    props.history.push('/showPreviousRequests')
  }

  if(userInfo.admin){
    return(
      <header className="header">
      <div className="header__logoWrapper">
        <img
          src={deltaLogo}
          alt="Delta Corp Shipping"
          className="header__logo"
        />
      </div>

      <h1 className="header__appTitle">DeltaQ</h1>
      {/* <h2 className="header__appTitle" 
      style={{position:'absolute',left:'1080px',top:'37px'}}>Welcome {localStorage.getItem('UserInfo')}</h2>
   <Link to='/logout' className='logout' style={{position:'absolute',left:'1250px',top:'33px'}}>
    <img title="Logout" src={logout} alt="" style={{width:28,height:28}}/></Link> */}

      <div style={{position:'absolute',right:'80px',top:'27px'}}
      className="header__userDropdown" onClick={toggleUserDropdown}>
        {userInfo.first_name}
        <object
          type="image/svg+xml"
          className={`header__userDropdownIndicator ${
            showUserDropdown ? "header__userDropdownIndicator--reversed" : ""
          }`}
          data={expandIcon}
        >
          ICON
        </object>
        <div
          className={`header__userDropdownContent ${
            showUserDropdown
              ? "header__userDropdownContent--shown"
              : "header__userDropdownContent--hidden"
          }`}
        >
           <button type="button" className="header__dropdownOption" onClick={handleAdminPage}>
            Admin
          </button>
          <button type="button" className="header__dropdownOption" onClick={handleChangePasswordPage}>
            Change Password
          </button>
          <button type="button" className="header__dropdownOption" onClick={handleNewCargo}>
            New Cargo
          </button>
          <button type="button" className="header__dropdownOption" onClick={handlePreviousCargo}>
            Previous Cargo
          </button>
          <button type="button" className="header__dropdownOption" onClick={handlePreviousResults}>
            Show Recent Logs
          </button>
          <button type="button" className="header__dropdownOption">
          <Link to='/logout' className="header__dropdownOption">
            Log Out
            </Link>
          </button>
        </div>
      </div>
    </header>
    )
  }
  else {
  return (
    <header className="header">
      <div className="header__logoWrapper">
        <img
          src={deltaLogo}
          alt="Delta Corp Shipping"
          className="header__logo"
        />
      </div>

      <h1 className="header__appTitle">DeltaQ</h1>
      {/* <h2 className="header__appTitle" 
      style={{position:'absolute',left:'1080px',top:'37px'}}>Welcome {localStorage.getItem('UserInfo')}</h2>
   <Link to='/logout' className='logout' style={{position:'absolute',left:'1250px',top:'33px'}}>
    <img title="Logout" src={logout} alt="" style={{width:28,height:28}}/></Link> */}

      <div style={{position:'absolute',right:'80px',top:'27px'}}
      className="header__userDropdown" onClick={toggleUserDropdown}>
        {userInfo.first_name}
        <object
          type="image/svg+xml"
          className={`header__userDropdownIndicator ${
            showUserDropdown ? "header__userDropdownIndicator--reversed" : ""
          }`}
          data={expandIcon}
        >
          ICON
        </object>
        <div
          className={`header__userDropdownContent ${
            showUserDropdown
              ? "header__userDropdownContent--shown"
              : "header__userDropdownContent--hidden"
          }`}
        >
           <button type="button" className="header__dropdownOption" onClick={handleChangePasswordPage}>
            Change Password
          </button>
          <button type="button" className="header__dropdownOption" onClick={handleNewCargo}>
            New Cargo
          </button>
          <button type="button" className="header__dropdownOption" onClick={handlePreviousCargo}>
            Previous Cargo
          </button>
          <button type="button" className="header__dropdownOption" onClick={handlePreviousResults}>
            Show Recent Logs
          </button>
          <button type="button" className="header__dropdownOption">
          <Link to='/logout' className="header__dropdownOption">
            Log Out
            </Link>
          </button>
        </div>
      </div>
    </header>
  )
        }
};

export default Header;
