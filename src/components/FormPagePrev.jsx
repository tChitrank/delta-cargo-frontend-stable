import React from 'react';

import Header from './Header';
// import Login from './Login';
import InputInterfacePrev from './InputInterfacePrev';

function FormPagePrev(props) {
  //console.log('props',props)
  return (
    <>
      <Header {...props}/>
      <InputInterfacePrev {...props}/>
    </>
  );
}

export default FormPagePrev;
