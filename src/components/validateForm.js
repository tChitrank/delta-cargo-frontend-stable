const validateForm = (values) => {
  
  const errors = {};

  const reqError = 'This field is required'
  const formatError = 'Please enter a valid value'

  var decimalRegex = /^[-+]?[0-9]+\.[0-9]+$/;
  var integerRegex = /^[0-9]+$/;
 // var charRegex = /^[A-Za-z]+$/;
  
  if (!values.freightRate) { errors.freightRate = reqError }  
  if (!values.shipSegment) { errors.shipSegment = reqError }
  if (!values.shipZone) { errors.shipZone = reqError }
  if (!values.layDate) { errors.layDate = reqError }
  if (!values.canDate) { errors.canDate = reqError }
  if (!values.cargoCategory) { errors.cargoCategory = reqError }
  if (!values.cargoQuantity) { errors.cargoQuantity = reqError }
  if (!values.benchMarkBalticIndex) { errors.benchMarkBalticIndex = reqError }
  if (!values.stowageFactor) { errors.stowageFactor = reqError }
  if (!values.cargoTolerance) { errors.cargoTolerance = reqError }
  if (!values.seaMargin) { errors.seaMargin = reqError }
  if (!values.commission) { errors.commission = reqError }
  if (!values.loadPort) { errors.loadPort = reqError }
  if (!values.loadPortDraft) { errors.loadPortDraft = reqError }
  if (!values.loadType) { errors.loadType = reqError }
  if (!values.dischargePort) { errors.dischargePort = reqError }
  if (!values.dischargePortDraft) { errors.dischargePortDraft = reqError }
  if (!values.dischargeType) { errors.dischargeType = reqError }
  if (!values.bunkerPort) { errors.bunkerPort = reqError }
  if (!values.bunkerDuration) { errors.bunkerDuration = reqError }
  if (!values.loadPortCost) { errors.loadPortCost = reqError }
  if (!values.dischargePortCost) { errors.dischargePortCost = reqError }
  if (!values.costBunkerPort) { errors.costBunkerPort = reqError }
  if (!values.fuelRate) { errors.fuelRate = reqError }
  if (!values.gasRate) { errors.gasRate = reqError }

  if (values.freightRate && !(values.freightRate.match(decimalRegex) ||
    values.freightRate.match(integerRegex))) { errors.freightRate = formatError }
 // if (values.cargoQuantity && !(values.cargoQuantity.match(decimalRegex) ||
 //   values.cargoQuantity.match(integerRegex))) { errors.cargoQuantity = formatError }
 // if (values.stowageFactor && !(values.stowageFactor.match(decimalRegex) ||
 //   values.stowageFactor.match(integerRegex))) { errors.stowageFactor = formatError }
  if (values.cargoTolerance && !(values.cargoTolerance.match(decimalRegex) ||
    values.cargoTolerance.match(integerRegex))) { errors.cargoTolerance = formatError }
  if (values.seaMargin && !(values.seaMargin.match(decimalRegex) ||
    values.seaMargin.match(integerRegex))) { errors.seaMargin = formatError }
  if (values.commission && !(values.commission.match(decimalRegex) ||
    values.commission.match(integerRegex))) { errors.commission = formatError }
  if (values.loadPortDraft && !(values.loadPortDraft.match(decimalRegex) ||
    values.loadPortDraft.match(integerRegex))) { errors.loadPortDraft = formatError }
  if (values.loadType && !(values.loadType.match(decimalRegex) ||
    values.loadType.match(integerRegex))) { errors.loadType = formatError }
  if (values.dischargePortDraft && !(values.dischargePortDraft.match(decimalRegex) ||
    values.dischargePortDraft.match(integerRegex))) { errors.dischargePortDraft = formatError }
  if (values.dischargeType && !(values.dischargeType.match(decimalRegex) ||
    values.dischargeType.match(integerRegex))) { errors.dischargeType = formatError }
 // if (values.bunkerPortDraft && !(values.bunkerPortDraft.match(decimalRegex) ||
 //   values.bunkerPortDraft.match(integerRegex))) { errors.bunkerPortDraft = formatError }
 // if (values.bunkerDuration && !(values.bunkerDuration.match(decimalRegex) ||
 //   values.bunkerDuration.match(integerRegex))) { errors.bunkerDuration = formatError }
 // if (values.fuelRate && !(values.fuelRate.match(decimalRegex) ||
 // values.fuelRate.match(integerRegex))) { errors.fuelRate = formatError }
 // if (values.gasRate && !(values.gasRate.match(decimalRegex) ||
 // values.gasRate.match(integerRegex))) { errors.gasRate = formatError }

  return errors;
};

export default validateForm;