import React, { useState } from 'react';
import Header from './Header';
import downloadReport from '../assets/excel.png'
import CsvDownload from 'react-json-to-csv'
import strings from '../assets/strings'
import { useEffect } from 'react';

function Results(props) {
  let data = JSON.parse(localStorage.getItem('responseData'))
  let dataCSV = JSON.parse(localStorage.getItem('responseData'))
  let values = JSON.parse(localStorage.getItem('requestData'))
  //console.log('formData',values)
  //console.log('resultdata',data)
  const [resultsData, updatedResultsData] = useState(data)
  const [resultsDataCSV, updatedResultsDataCSV] = useState(dataCSV)
  const [freightRate, updatedFreightRate] = useState(values.freight_rate);
  const [vesselHireRate, updatedVesselHireRate] = useState();
  const userData = JSON.parse(localStorage.getItem('UserInfo'))

  useEffect(() => {
    let array = resultsDataCSV.map((obj) => {
      for (let [key, value] of Object.entries(obj)) {
        if (key === 'age') {
          obj['Built Year'] = obj[key]
          delete obj[key]
        }
        if (key === 'date') {
          obj['Open Date'] = obj[key]
          delete obj[key]
        }
        if (key === 'eta_loadport') {
          let s = value
          let eta_value = s.replace('\n', ' ')
          obj.eta_loadport = eta_value
        }
      }
      return obj
    })
    updatedResultsDataCSV(array)
    //console.log('array',array)
  }, [])

  const checkNewResults = () => {
    let updatedVesselRateRequest = []
    updatedVesselRateRequest = resultsData
      .map(obj => ({
        vessel_hire_rate: parseFloat(vesselHireRate),
        name: obj.name,
        voyage_duration: obj.voyage_duration,
        breakeven_hire_rate: obj.breakeven_hire_rate.toString(),
      }))
    // console.log('vr:',updatedVesselRateRequest)
    fetch(strings.api_url + 'api/profit_vessel/', {
      method: 'POST',
      headers: {
        DELTAAUTH: userData.token,
        'Accept': 'application/json',
        'Content-type': 'application/json',
      },
      body: JSON.stringify(updatedVesselRateRequest)
    })
      .then(response => {
        if (!response || !response.ok) throw alert("Server Error");
        return response.json();
      })
      .then(responseData => {
        // console.log('dta',responseData);
        let updateResults = resultsData
          .map((obj, index) => ({
            ...obj,
            profit_loss: responseData.data[index].profit_loss,
          }))
        updatedResultsData(updateResults)
        updatedResultsDataCSV(updateResults)
      })
      .catch(err => console.log('Error Loggin in', err));

  }

  const checkFreightResults = () => {
    let updatedFrieghtRateRequest = []
    updatedFrieghtRateRequest = resultsData
                                   .map(obj => {
                                     if (obj.name==='Handy' || obj.name==='Supra' || obj.name==='Panamax')
                                       return {
                                         name: obj.name,
                                         freight_rate: parseFloat(obj.freight_rate),
                                         cargo_intake: obj.cargo_intake.toString(),
                                         commission: parseFloat(values.commission),
                                         bunkerport_cost: parseInt(values.bunkerport_cost),
                                         loadport_cost: parseInt(values.loadport_cost),
                                         dischargeport_cost: parseInt(values.dischargeport_cost),
                                         voyage_duration: parseFloat(obj.voyage_duration),
                                         fuel_cost: parseFloat(obj.fuel_cost),
                                         benchmark_baltic_index: parseInt(values.benchmark_baltic_index)
                                       }
                                      else 
                                      return {
                                        name: obj.name,
                                        freight_rate: parseFloat(freightRate),
                                        cargo_intake: obj.cargo_intake.toString(),
                                        commission: parseFloat(values.commission),
                                        bunkerport_cost: parseInt(values.bunkerport_cost),
                                        loadport_cost: parseInt(values.loadport_cost),
                                        dischargeport_cost: parseInt(values.dischargeport_cost),
                                        voyage_duration: parseFloat(obj.voyage_duration),
                                        fuel_cost: parseFloat(obj.fuel_cost),
                                        benchmark_baltic_index: parseInt(values.benchmark_baltic_index)
                                      }
                                   })
                                 
     console.log('vr:',updatedFrieghtRateRequest)
    fetch(strings.api_url + 'api/profit_freight/', {
      method: 'POST',
      headers: {
        DELTAAUTH: userData.token,
        'Accept': 'application/json',
        'Content-type': 'application/json',
      },
      body: JSON.stringify(updatedFrieghtRateRequest)
    })
      .then(response => {
        if (!response || !response.ok) throw alert("Server Error");
        return response.json();
      })
      .then(responseData => {
        // console.log('dta',responseData);
        let updateResults = resultsData
          .map((obj, index) => ({
            ...obj,
            freight_rate: responseData.data[index].freight_rate,
            breakeven_hire_rate: responseData.data[index].breakeven_hire_rate,
            percentage_index: responseData.data[index].percentage_index,
          }))
        updatedResultsData(updateResults)
        updatedResultsDataCSV(updateResults)
      })
      .catch(err => console.log('Error Loggin in', err));
  
  }

  let color = "#eaeaea"
  const propsData = resultsData
    .map(obj => {
      let s = obj.profit_loss.toString()
      if (obj.name === 'Handy' || obj.name === 'Supra' || obj.name === 'Panamax') {
        color = "#FFFF33"
      }
      else if (s.charAt(0) === '-') {
        color = '#FFCCCB'
      }
      else {
        color = "#eaeaea"
      }

      return (
        <div style={{
          backgroundColor: color, height: 64,
          margin: 20, display: 'flex', marginBottom: 16, marginTop: 16
        }}>
          <p style={{ marginTop: 10, position: 'absolute', left: '3%', fontSize: 16 }}>{obj.name}<br />(<strong>{obj.fuel_type}</strong>)</p>
          <p style={{ marginTop: 10, position: 'absolute', left: '14%', fontSize: 16 }}>{obj.age}</p>
          <p style={{ marginTop: 10, position: 'absolute', left: '20%', fontSize: 16 }}>{obj.deadweight}</p>
          <p style={{ marginTop: 10, position: 'absolute', left: '27%', fontSize: 16 }}>{obj.open_port}</p>
          <p style={{ marginTop: 10, position: 'absolute', left: '36%', fontSize: 16 }}>{obj.date}</p>
          <p style={{ marginTop: 10, position: 'absolute', left: '47%', fontSize: 16, whiteSpace: 'pre-wrap' }}>{obj.eta_loadport}</p>
          <p style={{ marginTop: 10, position: 'absolute', left: '58%', fontSize: 16 }}>{obj.cargo_intake}</p>
          <p style={{ marginTop: 10, position: 'absolute', left: '65.5%', fontSize: 16 }}>{parseInt(obj.voyage_duration)}</p>
          <p style={{ marginTop: 10, position: 'absolute', left: '71%', fontSize: 16 }}>
            {obj.percentage_index + "%"}</p>
          <p style={{ marginTop: 10, position: 'absolute', left: '77.5%', fontSize: 16 }}>${obj.breakeven_hire_rate}</p>
          <p style={{ marginTop: 10, position: 'absolute', left: '87%', fontSize: 16 }}>{obj.freight_rate}</p>
          <p style={{ marginTop: 10, position: 'absolute', left: '92%', fontSize: 16 }}>${obj.profit_loss}</p>
        </div>
      )
    })
  return (
    <>
      <Header {...props} />

      <div>
        <div
          style={{ display: 'flex', margin: 10, marginLeft: 18, width: 104, alignItems: 'center' }}>
          <img src={downloadReport} alt=""
            style={{ width: 44, height: 44 }} />
          <CsvDownload
            className="downloadReport"
            data={resultsDataCSV}
            filename='results.csv'
            style={{
              border: 'none',
              outline: 'none',
              textAlign: 'center',
              textDecoration: 'none',
              display: 'inline-block',
              cursor: 'pointer'
            }}
          >
            <p style={{ fontSize: '1.1rem' }}>Download<br /> Report</p>
          </CsvDownload>
        </div>

        <button type="submit" className="submitButton" onClick={() => {
          updatedResultsData(data)
          window.location.reload()
        }}
          style={{ marginTop: 10, position: 'absolute', top: 90, left: 135 }}>Show Original Results</button>

        <p className="inputLabel"
          style={{ marginTop: 10, position: 'absolute', top: 100, right: 576, fontSize: 16 }}>Vessel Hire Rate</p>
        <input
          type="text"
          name="vesselHireRate"
          defaultValue={vesselHireRate}
          id="vesselHireRate"
          className="inputField"
          onChange={() => { updatedVesselHireRate(document.getElementById("vesselHireRate").value) }}
          style={{ marginTop: 10, position: 'absolute', top: 92, right: 482, fontSize: 16, width: 85 }}
        />
        <button type="submit" className="submitButton" onClick={() => { checkNewResults() }}
          style={{ marginTop: 10, position: 'absolute', top: 90, right: 372 }}>Check</button>

        <p className="inputLabel"
          style={{ marginTop: 10, position: 'absolute', top: 100, right: 262, fontSize: 16 }}>Freight Rate</p>
        <input
          type="text"
          name="freightRate"
          defaultValue={freightRate}
          id="freightRate"
          className="inputField"
          onChange={() => { updatedFreightRate(document.getElementById("freightRate").value) }}
          style={{ marginTop: 10, position: 'absolute', top: 92, right: 172, fontSize: 16, width: 85 }}
        />
        <button type="submit" className="submitButton" onClick={() => { checkFreightResults() }}
          style={{ marginTop: 10, position: 'absolute', top: 90, right: 62 }}>Check</button>
      </div>


      <p style={{ fontSize: 24, margin: 20, marginTop: 8, marginBottom: 8, fontWeight: 'bold' }}>RESULTS</p>
      <br />
      <div style={{ height: '20px', width: '40px', position: 'absolute', top: 184, marginLeft: 20, backgroundColor: '#FFFF33' }} />
      <p className="inputLabel"
        style={{ marginTop: 10, position: 'absolute', top: 172, left: '5%', fontSize: 16 }}>Benchmark Vessel </p>
      <p className="inputLabel"
        style={{ marginTop: 10, position: 'absolute', top: 170, right: '16%', fontSize: 16 }}>Baltic Index Value: </p>
      <p style={{ marginTop: 10, position: 'absolute', top: 170, right: '5%', fontSize: 16 }}>
        ${values.benchmark_baltic_index} ({values.baltic_type}) </p>

      <div style={{
        backgroundColor: '#202020', height: 84,
        margin: 20, marginTop: 8, display: 'flex', marginBottom: 25
      }}>
        <p style={{ color: 'white', fontWeight: 'bold', fontSize: 16, textAlign: 'center', position: 'absolute', top: 229, left: '3%' }}>Vessel<br /> (Fuel Type)</p>
        <p style={{ color: 'white', fontWeight: 'bold', fontSize: 16, textAlign: 'center', position: 'absolute', top: 229, left: '14%' }}>Built<br /> (Year)</p>
        <p style={{ color: 'white', fontWeight: 'bold', fontSize: 16, textAlign: 'center', position: 'absolute', top: 229, left: '20%' }}>Dwt <br /> (MTS)</p>
        <p style={{ color: 'white', fontWeight: 'bold', fontSize: 16, textAlign: 'center', position: 'absolute', top: 229, left: '28%' }}>Open<br /> Port</p>
        <p style={{ color: 'white', fontWeight: 'bold', fontSize: 16, textAlign: 'center', position: 'absolute', top: 225, left: '35%' }}>Open <br />Date<br /> (DD-MON-YYYY)</p>
        <p style={{ color: 'white', fontWeight: 'bold', fontSize: 16, textAlign: 'center', position: 'absolute', top: 225, left: '47%' }}>ETA <br /> Loadport<br /> (DD-MON-YYYY)</p>
        <p style={{ color: 'white', fontWeight: 'bold', fontSize: 16, textAlign: 'center', position: 'absolute', top: 225, left: '58%' }}>Cargo <br />Intake<br /> (MTS)</p>
        <p style={{ color: 'white', fontWeight: 'bold', fontSize: 16, textAlign: 'center', position: 'absolute', top: 225, left: '64%' }}>Voyage<br /> Duration <br /> (DAYS)</p>
        <p style={{ color: 'white', fontWeight: 'bold', fontSize: 16, textAlign: 'center', position: 'absolute', top: 225, left: '71%' }}>%<br /> Index </p>
        <p style={{ color: 'white', fontWeight: 'bold', fontSize: 16, textAlign: 'center', position: 'absolute', top: 225, left: '77%' }}>B/E<br /> Hire Rate <br /> ($ / DAY)</p>
        <p style={{ color: 'white', fontWeight: 'bold', fontSize: 16, textAlign: 'center', position: 'absolute', top: 225, left: '84%' }}>B/E<br />Freight Rate <br /> ($ / MT)</p>
        <p style={{ color: 'white', fontWeight: 'bold', fontSize: 16, textAlign: 'center', position: 'absolute', top: 225, left: '93%' }}>Profit/<br />Loss <br /> ($)</p>

      </div>

      {propsData}
    </>
  )
}

export default Results;