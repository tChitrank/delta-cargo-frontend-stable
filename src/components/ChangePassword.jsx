import React, { useState } from 'react';
import Header from './Header';
import AdminImg from '../assets/admin.png'
import { toast } from 'react-toastify';
import {successOptions} from '../toastConfig'
import strings from '../assets/strings'

function ChangePassword(props) {
    let [errors, updateErrors] = useState({
        email: '',
        old_password: '',
        new_password: '',
    })
    const userData = JSON.parse(localStorage.getItem('UserInfo'))

    const onSubmit = (e) => {
        e.preventDefault()
        let error = errors;
        
        if(document.getElementById('emailId').value.length === 0 ){
            error.email = 'Please enter a valid email!'
            updateErrors({...errors,email:error.email})
        }
        if(document.getElementById('old_password').value.length === 0 ){
            error.old_password = 'Please enter a valid value!'
            updateErrors({...errors,old_password:error.old_password})
        }
        if(document.getElementById('new_password').value.length === 0 ){
            error.new_password = 'Please enter a valid value!'
            updateErrors({...errors,new_password:error.new_password})
        }
        
        console.log('token',document.getElementById('emailId').value)
        console.log('errors', document.getElementById('old_password').value)
        console.log('errors', document.getElementById('new_password').value)

           fetch(strings.api_url+`change_password/`, {
            method: 'POST',
            headers: {
               DELTAAUTH: userData.token,
              'Accept': 'application/json',
              'Content-type': 'application/json',
            },
            body: JSON.stringify({
                "email"       :document.getElementById('emailId').value,
                "old_password":document.getElementById('old_password').value,
                "new_password":document.getElementById('new_password').value
            })
          })
            .then(response => {
              if (!response || !response.ok) throw alert("Check Internet Connection");
              return response.json();
            })
            .then(data => {
             // console.log("data:",data.data)
             toast.success("Password Changed Successfully!",successOptions)
             props.history.push('/form')
             //alert("Successfully Entered")
            })
            .catch(err => console.log('Error Retreiving', err)); 
    }

    return (
        <>
            <Header {...props} />

            <div style={{ display: 'flex', flexDirection: 'row' }}>
                <img
                    src={AdminImg}
                    alt="Admin"
                    className="header__logo"
                />

                <form style={{ marginLeft: 20, width: '50%' }} onSubmit={onSubmit} >
                    <div className="wrap-input100">
                        <input className="input100" type="text" id="emailId" name="emailId" style={{ border: 'none', outline: 'none' }}
                            placeholder="Email ID" autoComplete="off" noValidate/>
                             {errors.email.length > 0 ? 
                <span style={{ color: 'red' }}>{errors.email}</span> : ''}
                        <span className="focus-input100"></span>
                    </div>

                    <div className="wrap-input100">
                        <input className="input100" type="text" id="old_password" name="old_password" style={{ border: 'none', outline: 'none' }}
                            placeholder="Old Password" autoComplete="off" noValidate/>
                             {errors.old_password.length > 0 ? 
                <span style={{ color: 'red' }}>{errors.old_password}</span> : ''}
                        <span className="focus-input100"></span>
                    </div>

                    <div className="wrap-input100">
                        <input className="input100" type="text" id="new_password" name="new_password" style={{ border: 'none', outline: 'none' }}
                            placeholder="New Password" autoComplete="off" noValidate/>
                             {errors.new_password.length > 0 ? 
                <span style={{ color: 'red' }}>{errors.new_password}</span> : ''}
                        <span className="focus-input100"></span>
                    </div>



                    <button type="submit" className="submitButton"
                        style={{ marginTop: 20 }}>Submit</button>
                </form>

            </div>

        </>
    )
}

export default ChangePassword