export const STOWAGE_FACTOR = [ 'cub. ft/MT'
//,'cub. m/MT'
];
export const DRAFTS = [
  { text: "SW", key: "Salt(1.025)" },
  { text: "BW", key: "Brackish(1.012)" },
  { text: "FW", key: "Fresh(1.000)" },
];
export const DURATION_RATE_TYPES = ['MT/day', 
'days'
];
/*export const TERMS = [
  'SHINC',
  'SHEX0',
  'SHEX1',
  'SHEX2',
  'SHEX3',
  'SHEX4',
  'SHEX5',
  'SHEX6',
  'FHINC',
  'FHEX1',
  'FHEX2',
  'CQD1',
  'SHEX7',
  'SHEX8',
  'FHEX3',
  'THEX9',
  'FHEX4',
  'SHEX9',
  'FHEX5',
]; */
export const PORT_TERMS = [
  { text: "SHINC", key: "Sun. & Holiday Incl.(1.000)" },
  { text: "SHEX0", key: "Sun. & Holiday Excl.(1.167)" },
  { text: "SHEX1", key: "Sat.1200 to Sun.2400 Excl.(1.273)" },
  { text: "SHEX2", key: "Sat.1200 to Mon. 0900 Excl.(1.355)" },
  { text: "SHEX3", key: "Fri.2400 to Sun.2400 Excl.(1.400)" },
  { text: "SHEX4", key: "Fri.1700 to Sun. 2400 Excl.(1.487)" },
  { text: "SHEX5", key: "Fri.2400 to Mon. 0800 Excl.(1.500)" },
  { text: "SHEX6", key: "Fri.1700 to Mon.0800 Excl.(1.600)" },
  { text: "FHINC", key: "Fri. & Holiday Incl.(1.000)" },
  { text: "FHEX1", key: "Fri. & Holiday Excl.(1.167)" },
  { text: "FHEX2", key: "Wed.1700 to Sat. 0800 Excl.(1.600)" },
  { text: "CQD1",  key: "Customary quick despatch SHINC(1.000)" },
  { text: "SHEX7", key: "Thu.1700 to Sun.0800 Excl.(1.600)" },
  { text: "SHEX8", key: "Thu.1200 to Sat.0800 Excl.(1.355)" },
  { text: "FHEX3", key: "Fri.1200 to Sun.0800 Excl.(1.355)" },
  { text: "THEX9", key: "Thu.1200 to Sun.0800 Excl.(1.680)" },
];

export const VESSEL_TYPE = ['Normal-Fuel','Eco-Fuel']

export const PORTS = ['rizhao', 'santos', 'kolkata', 'santa cruz (Argentina)', 'urini', 'van gogh','singapore', 'fujairah'];

export const BUNKER_PORTS = [{label:"GIBRALTAR", value:"GIBRALTAR"}, 
{label:"HAMBURG",value:"HAMBURG"}, 
{label:"ROTTERDAM",value:"ROTTERDAM"}, 
{label:"SINGAPORE",value:"SINGAPORE"}, 
{label:"FUJAIRAH",value:"FUJAIRAH"}, 
{label:"DURBAN",value:"DURBAN"}, 
{label:"HONG KONG",value:"HONG_KONG"}];

export const CARGO = ["AGGREGATES","ALUMINA","ANTHRACITE","BAGGED UREA","BARLEY","BARYTES","BASE OIL",
"BAUXITE","BENTONITE ","BITUMEN","BLAST FURNACE COKE","BULK HARMLESS FERT","CANOLA MEAL","CEMENT",
"CHROME ORE","CLAY","CLINKER","COAL","COKE BREEZE","CONCENTRATES","COPPER CONCENTRATE","COPPER SLAG","CORN",
"CORN BAGGED ","CORN GLUTEN FEED PELLETS","DDG","DI AMMONIUM PHOSPHATE","DI AMMONIUM PHOSPHATE BAGGED",
"DIRECT REDUCED IRON","FELDSPAR","FERTILIZER","FLUROSPAR","FLY ASH","GASOIL","GRAINS / GRAIN PRODS 49",
"GRAINS 51","GRANITE BLOCKS","GRANULAR UREA","GSSP","GYPSUM IN BULK","HSS","IRON ORE CONCENTRATES",
"IRON ORE FINES","IRON ORE PELLETS","JET FUEL","LAMETCOKE","LATERITE","LIMESTONE","LOGS","MAIZE",
"MAIZE BAGGED","MANGANESE ORE","MDF","METCOKE","METCOKE BREEZE","MILLED RICE","MILLSCALE","MISC GRAIN PRODS",
"MOP","NATURAL ANHYDRITE ","NEW ZEALAND LOGS","NICKEL CONCENTRATES ","NUTCOKE ","OATS","ORES","PALM KERNEL SHELLS",
"PEAS","PET COKE","PHOSPHATE ROCK","PIG IRON","PIPE","PKE","POTASH","RAPE SEEDS","RICE IN BAGS","ROCK PHOS","SALT",
"SCRAP","SCRAP XMBT","SILICA SAND","SLAG","SODA ASH","SODA FELDSPAR","SORGHUM","SOYABEAN","SOYABEAN BAGGED",
"SOYABEEN MEAL","SP 36","SPONDUMENE","STEEL BILLETS","STEEL BLOOMS AND COILS","STEEL COILS","STEEL PIPES",
"STEELBILLETS AND PIG IRON","STONE CHIPS ","SUGAR BAGGED","SUGAR BEET PULP PELLETS","SUGAR BULK","SULPHUR",
"SUNFLOWER PELLETS","SUNFLOWER SEEDS","SURINAME LOGS","TAPIOCA","TIMBER","UREA","UREA BAGGED","URUGUAYAN PINE LOGS",
"VANIMO LOGS","WHEAT","WHEAT BAGGED ","WOOD PELLETS","WOODCHIPS","ZINC CONCENTRATES"];

export const SEGMENT = ['Handy', 'Supra', 'Panamax'];

export const ZONE = ['S East Asia', 'Far East', 'India', 'NOPAC & USWC', 'Aussie & NZ', 'PG & Red Sea', 'Med & Bl.Sea',
 'Continent', 'ECSA + S.Atln + SW Africa', 'South Africa + IOR + S.Ind Ocean', 'US Gulf & USEC'];

/*export const BBI_HANDY = ['BHSI-7TC', 'HS1_38', 'HS2_38', 'HS3_38',
'HS4_38', 'HS5_38', 'HS6_38', 'HS7_38'];

export const BBI_SUPRA = ['BSI-10TC', 'S1B_58', 'S1C_58', 'S2_58',
'S3_58','S4A-58','S5_58','S8_58','S9_58','S10_58','S11_58'];

export const BBI_PANAMAX = ['BPI_825TC','P1A_82','P2A_82','P3A_82',
'P4A_82','P6_82','P5_82'] */

export const BBI_PANAMAX_INDEX =  [
  { text: "BPI_825TC", key: "average" },
  { text: "P1A_82", key: "Skaw-Gib TA RVs" },
  { text: "P2A_82", key: "Skaw-Gib trip HK-S Korea" },
  { text: "P3A_82", key: "HK-S Korea  PAC. RVs" },
  { text: "P4A_82", key: "HK-S Korea trip to Skaw-Gib" },
  { text: "P6_82",  key: "Dely Spore or Busan (during US grain season) RV via Atl." },
  { text: "P5_82",  key: "S.China -Indo RV" },
];

export const BBI_SUPRA_INDEX = [
  { text: "BSI-10TC", key: "average" },
  { text: "S1B_58", key: "Med or Bl Sea to F.East" },
  { text: "S1C_58", key: "USG to F.East" },
  { text: "S2_58", key: "N.China Aussie or Pac. RV" },
  { text: "S3_58", key: "N.China to W.Africa" },
  { text: "S4A-58", key: "USG to Skaw-Passero" },
  { text: "S4B-58", key: "Skaw-Passero trip to USG" },
  { text: "S5_58", key: "W. Africa trip Via  ECSA N.China" },
  { text: "S8_58", key: "S.China trip Via Indo -ECI" },
  { text: "S9_58", key: " W. Africa trip Via  ECSA -skaw-passero" },
  { text: "S10_58", key: "South China - Indo RV" },
  { text: "S11_58", key: "Mid China, Aussie-Pac RV" },
];

export const BBI_HANDY_INDEX = [
  { text: "BHSI-7TC", key: "average" },
  { text: "HS1_38", key: "Skaw-Passero trip to ECSA" },
  { text: "HS2_38", key: "Skaw-Passero trip to USG" },
  { text: "HS3_38", key: "ECSA trip to Skaw-Passero" },
  { text: "HS4_38", key: "USG trip via USG or NCSA to Skaw-Passero" },
  { text: "HS5_38", key: "S.E Asia  trip to F.East" },
  { text: "HS6_38", key: "F.East trip to F.East" },
  { text: "HS7_38", key: "F.East  trip to S.E. Asia" },
];