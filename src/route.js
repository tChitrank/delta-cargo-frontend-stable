import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Login from './components/Login'
import FormPage from './components/FormPage'
import FormPagePrev from './components/FormPagePrev'
import Results from './components/Results'
import ShowPrevious from './components/ShowPrevious'
import Logout from './components/logout'
import Admin from './components/Admin'
import ChangePassword from './components/ChangePassword'

function Routing() {
  return (
      <Switch>
        <Route exact path='/' render={props=> <Login {...props}/>}  />
        <Route exact path='/form' render={props=> <FormPage {...props}/>}/>
        <Route exact path='/formPrev' render={props=> <FormPagePrev {...props}/>}/>
        <Route exact path='/admin' render={props=> <Admin {...props}/>}/>
        <Route exact path='/changePassword' render={props=> <ChangePassword {...props}/>}/>
        <Route exact path='/showPreviousRequests' render={props=> <ShowPrevious {...props}/>}/>
         <Route exact path='/results' render={props=> <Results {...props}/>}/> 
         <Route path='/logout' render={ ()=> <Logout/>}/> 
      </Switch>
  );
}

export default Routing;
